<?php 
	session_start();
	include_once("controller/functions/elements.php");
	$path_only = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php 

    	include_once("reference.php");	
		luball_element("head.php","Urban City designs");

		

		if(!empty($_SESSION)){
			echo "<script> localStorage.setItem('sd','".json_encode($_SESSION)."');</script>\n";
		}else{
			echo "<script> localStorage.removeItem('sd');</script>";
		}
	?>

	<meta property="og:title" content="Urban City Designs">
	<meta property="og:description" content="We are committed to producing the highest quality of custom stone work and mosaics for commercial and residential projects, crafted by hand, from the finest materials around the world.">
	<meta property="og:url" content="https://urbancitydesigns.com">
	<meta property="og:image" content="https://ucddevops.000webhostapp.com/media/img/logo_d.png"> 
	<meta property="og:type" content="website">

	<meta name="description" content="">
    
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/index.css">

	<!-- Components -->

  </head>
  <body>
  	
	<div id="app">
		
		<b-navbar id="headerNav" class="onTop" toggleable="md" type="dark" :fixed="'top'" variant="info">

			<b-navbar-brand href="#" v-on:click="updateRoute('/')" style="padding-left: 3%;">
				<img src="web/img/logo.png" class="d-inline-block logo" alt="Urban City Designs" />
			</b-navbar-brand>

			<b-navbar-toggle target="nav_collapse"></b-navbar-toggle>

			<b-collapse center is-nav id="nav_collapse">

				<b-navbar-nav class="mx-auto">
					<b-nav-item href="#" v-on:click="updateRoute('/about')">About us</b-nav-item>
					<b-nav-item-dropdown
				      id="my-nav-dropdown"
				      text="Collections"
				      extra-toggle-classes="nav-link-custom"
				      right
				    >
				      <b-dropdown-item @click="updateRoute('/collections',1)">Coastline</b-dropdown-item>
				      <b-dropdown-item @click="updateRoute('/collections',2)">Progressive</b-dropdown-item>
				      <b-dropdown-item @click="updateRoute('/collections',3)">Timeless</b-dropdown-item>
				      <b-dropdown-item @click="updateRoute('/collections',4)">Traditional</b-dropdown-item>
				      <b-dropdown-item @click="updateRoute('/collections',5)">Geometric</b-dropdown-item>
				    </b-nav-item-dropdown>

				    <b-nav-item-dropdown
				      id="my-nav-dropdown"
				      text="Materials"
				      extra-toggle-classes="nav-link-custom"
				      right
				    >
				      <b-dropdown-item @click="updateRoute('/materials',1)">Stone</b-dropdown-item>
				      <b-dropdown-item @click="updateRoute('/materials',1)">Mirror</b-dropdown-item>
				      <b-dropdown-item @click="updateRoute('/materials',1)">Metal</b-dropdown-item>
				      <b-dropdown-item @click="updateRoute('/materials',1)">Glass</b-dropdown-item>
				      <b-dropdown-item @click="updateRoute('/materials',1)">Gemstones</b-dropdown-item>
				    </b-nav-item-dropdown>

				    <b-nav-item-dropdown
				      id="my-nav-dropdown"
				      text="Gallery"
				      extra-toggle-classes="nav-link-custom"
				      right
				    >
				      <b-dropdown-item @click="updateRoute('/catalog')">Catalog</b-dropdown-item>
				      <b-dropdown-item @click="updateRoute('/gallery')">Inspiration</b-dropdown-item>
				    </b-nav-item-dropdown>

				  <b-nav-item href="#" v-on:click="updateRoute('/contact')">Contact</b-nav-item>
				  
				  

				</b-navbar-nav>
				
				<b-navbar-nav>
					<b-nav-item v-if="!session.status" v-b-modal="'loginModal'">Login</b-nav-item>
				  
					  <b-nav-item-dropdown  v-if="session.status" style="margin-right: 70px!important">
					    <!-- Using button-content slot -->
					    <template slot="button-content">
					      {{ session.name }}&nbsp;{{ session.lastname }}
					    </template>
					    <b-dropdown-item href="dashboard">Dashboard</b-dropdown-item>
					    <b-dropdown-item href="logout">Signout</b-dropdown-item>
					  </b-nav-item-dropdown>
				</b-navbar-nav>

				

			</b-collapse>
		</b-navbar>

		<cart-order ref="cartSave" id="cartSave"></cart-order>
		<route-view></route-view>
		
		<b-modal ref="loginModal" centered  title="SIGN UP OR CREATE AN ACCOUNT" id="loginModal">
			<session-form></session-form>
			<div slot="modal-footer" class="w-100">
			</div>
		</b-modal>
		
		
	</div>

	<l-footer id="footer"></l-footer>
	
	<script type='text/javascript' src='js/components/l-navbar.js'></script>
	<script type='text/javascript' src='js/components/catalog.js'></script>
	<script type='text/javascript' src='js/components/detail-order.js'></script>
	<script type='text/javascript' src='js/components/cart.js'></script>
	<script type='text/javascript' src='js/components/lmaterial.js'></script>
	<script type='text/javascript' src='js/components/lcollection.js'></script>

	<script type="text/javascript" src="js/components/carousel.js"></script>
	<script type="text/javascript" src="js/components/eula.js"></script>
    <script type="text/javascript" src="js/components/about.js"></script>
    <script type="text/javascript" src="js/components/home.js"></script>
    <script type="text/javascript" src="js/components/forgot.js"></script>
    <script type="text/javascript" src="js/components/gallery.js"></script>
    <script type="text/javascript" src="js/components/contact.js"></script>
    <script type="text/javascript" src="js/components/signup-form.js"></script>
    <script type="text/javascript" src="js/components/login-form.js"></script>
    <script type="text/javascript" src="js/components/session.js"></script>
	<script type="text/javascript" src="js/components/dashboard/building.js"></script>

    <script type="text/javascript" src="js/index.js"></script>

  </body>
</html>