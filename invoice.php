<?php 
	//CORS Policy declatarion

    //print_r(empty($_GET));
	/*
	var_dump($_SERVER['REQUEST_METHOD']);
	echo "<br> GET dump: <br>";
	var_dump($_GET);
	echo "<br> POST dump: <br>";
	var_dump($_POST);
	echo "<br>";
	echo "Array POST: ".var_dump(empty($_POST));
	echo "<br>";
	echo "Array Get: ".var_dump(empty($_GET));
	echo "<br>";*/

	if(empty($_GET) && empty($_POST)){
		include "dashboard.php";
	}else{
		session_start();
		header('Access-Control-Allow-Origin: *');  
	    header('Content-Type: application/json;charset=utf-8');
	    header('Vary: Accept-Encoding');

		include("model/connection.php");
		include("model/FriendofMySQL.php");
		include("controller/classes/luball.php");
		include("controller/classes/lfile.php");
		include("controller/classes/luballcg.php");

		$db = new FriendofMySQL($connection);
		$lb = new Luball();
		$lf = new lfile();
		$callback = array();
		$errors = array();

		$lb->power_session_start();

		if(!empty($_GET)){
			$method = "GET";
		}else{
			$method = (isset($_POST["method"]) ? $_POST["method"] : "400");
		}

		switch (strtoupper($method)) {
			case 'GET':

				switch($_GET["r"]) {
					case "a":
						if($lb->isAdmin()){
							$q = "SELECT id,`number`, pdf, DATE_FORMAT(dated, '%m - %d  - %y') as dated, description FROM invoice WHERE client = ".$_GET["i"];
						}else{
							$q = "SELECT id,`number`, pdf, DATE_FORMAT(dated, '%m - %d  - %y') as fdated, description FROM invoice WHERE client = ".$_SESSION['user']["id"];
						}

						$tryQ = $db->query($q,true,true);

						if($tryQ["status"]){
							$lb->toClient(true,$tryQ["query"]);
						}else{
							$lb->toClient(false,$tryQ["query"],"NotUploaded");
						}
					break;

					case "m":
						if($lb->isAdmin()){
							$get = "SELECT *, DATE_FORMAT(invoice.dated, '%m - %d  - %y') as fdated, user.id as uid, invoice.id as id FROM invoice,user WHERE user.id = invoice.client  ORDER BY invoice.dated DESC LIMIT 10";

							$tryQ = $db->query($get,true,true);

							if($tryQ["status"]){
								$lb->toClient(true,$tryQ["query"]);
							}else{
								$lb->defaultQuery();
							}
						}else{
							$lb->defaultPermission();
						}
					break;
					
					default:
						$sql = "SELECT *, LPAD(invoice.`number`, 6, '0') as `number`,  DATE_FORMAT(invoice.dated, '%m - %d  - %y') as fdated, user.id as uid, invoice.id as id FROM invoice,user WHERE invoice.client = user.id AND ((invoice.`number` LIKE '%".$_GET['r']."%') OR (CONCAT(user.name,' ',user.lastname,' ',user.business) LIKE '%".$_GET['r']."%' )) LIMIT 10";

						$list = $db->query($sql,true);

						if($list["status"]){
							$lb->toClient(true,$list['query']);
						}else{
							$lb->defaultQuery();
						}
					break;
				}

			break;

			case 'POST':
				//Post request handler

				switch ($_POST["request"]) {
					case 'add':

						if($lb->isAdmin()){
							$ex = array('desc' => "");
							if($lb->isSEA($_POST,$ex) && !empty($_FILES)){
								if($lf->cf($_FILES['file'],"pdf")){

									$client = $db->queryOneValue("orders",'client','number',$_POST['number']);

									if(!$client){
										$lb->toClient(false,$callback,"NotRealClient");
										return false;
									}	
									
									$pdf = $lf->mf($_FILES['file'],"files/".$lb->encrypt($client)."/invoices",$_FILES['file']["name"]);

									if($db->isValue("invoice","number",$_POST["number"])){
										$lb->toClient(false,$callback,"NumberAlready");
										return false;
									}

									

									if($pdf["answer"]){
										$inP = "INSERT INTO invoice (`number`, 
																	pdf, 
																	description, 
																	client, 
																	uploader, 
																	dated) 
															VALUES ('".$_POST['number']."',
																	'".$pdf['dir_name']."',
																	'".(isset($_POST['desc']) ? $_POST['desc'] : "")."',
																	'".$client."',
																	'".$_SESSION['user']['id']."',
																	CURRENT_TIMESTAMP)";
										$tryQ = $db->query($inP,false,true);

										if($tryQ["status"]){
											$lb->toClient(true,$callback,false);
										}else{
											$lb->toClient(false,$callback,"NotUploaded");
										}
									}
								}else{
									$lb->toClient(false,$callback,"NotPDF");
								}
								
							}else{
								$lb->toClient(false,$callback,"NotEnoughForm");
							}
							
						}else{
							$lb->toClient(false,$callback,"NotPermission");
						}
					break;

					case "delete":
						if($lb->isAdmin()){
							$sq = "DELETE FROM invoice WHERE id = ".$_POST["id"];
							$dl = $db->query($sq,false);

							if($dl["status"]){
								$lb->toClient(true,$callback);
							}else{
								$lb->defaultQuery();
							}
						}else{
							$lb->defaultPermission();
						}
						
					break;
					
					default:
						$lb->defaultRequest();
					break;
				}
			break;

			case 'PUT':
				//Post handled like PUT

			break;

			case 'DELETE':
				//Post handled like Delete

			break;
			
			default:
				$lb->defaultMethod($method);
			break;
		}

		$db->close($connection);

		//$db = new FriendofMySQL($execute);
	} 
	//print_r($_GET);
	//Classic HTTP method request hangling

	//Third FOM MySQL Server connection 
	

?>