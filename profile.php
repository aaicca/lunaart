<?php 
	//CORS Policy declatarion

    //print_r(empty($_GET));
	/*
	var_dump($_SERVER['REQUEST_METHOD']);
	echo "<br> GET dump: <br>";
	var_dump($_GET);
	echo "<br> POST dump: <br>";
	var_dump($_POST);
	echo "<br>";
	echo "Array POST: ".var_dump(empty($_POST));
	echo "<br>";
	echo "Array Get: ".var_dump(empty($_GET));
	echo "<br>";*/

	if(empty($_GET) && empty($_POST)){
		include "dashboard.php";
	}else{
		session_start();
		header('Access-Control-Allow-Origin: *');  
	    header('Content-Type: application/json;charset=utf-8');
	    header('Vary: Accept-Encoding');

		include("model/connection.php");
		include("model/FriendofMySQL.php");
		include("controller/classes/luball.php");
		include("controller/classes/lfile.php");
		include("controller/classes/luballcg.php");

		$db = new FriendofMySQL($connection);
		$lb = new Luball();
		$lf = new lfile();
		$callback = array();
		$errors = array();

		if(!empty($_GET)){
			$method = "GET";
		}else{
			$method = (isset($_POST["method"]) ? $_POST["method"] : "400");
		}

		switch (strtoupper($method)) {
			case 'GET':
			
				switch ($_GET["g"]) {
					case 'me':
						$lb->power_session_start();

						$sql = "SELECT * FROM user WHERE id = ".$_SESSION['user']['id'];

						$user = $db->query($sql,true,true);

						unset($user["query"][0]["pass"]);

						if($user["status"]){
							$lb->toClient(true,$user["query"]);
						}else{
							$lb->defaultQuery();
						}
						
					break;
					
					default:
						$lb->defaultRequest();
					break;
				}

			break;

			case 'POST':
				switch ($_POST["request"]) {
					case "update":
						$lb->power_session_start();

						unset($_POST["deleted"]);
						unset($_POST["img"]);
						unset($_POST["request"]);
						unset($_POST["method"]);
						unset($_POST["dated"]);
						unset($_POST["method"]);
						unset($_POST["hash"]);	
						unset($_POST["file"]);

						if(isset($_POST["pass1"]) && isset($_POST["pass2"])){
							if($_POST["pass1"] != $_POST["pass2"]){
								$lb->toClient(false,$callback,"NotMatchPass");
								return false;
							}else{
								$pass = $_POST["pass1"];
							}
							
						}

						foreach ($_POST as $key => $value) {
							if(empty($value) || $value == "null"){
								if($key != "pass1" && $key != "pass2"){
									$_POST[$key] = $_SESSION["user"][$key];
								}
								
							}
						}

						extract($_POST);	

						if(!empty($_FILES)){
							$_POST["img"] = $lf->mf($_FILES["file"],"files/".$lb->encrypt($_POST["id"])."/","profile.webp");
						}

						$upd = "UPDATE user SET name = '".$name."',
												lastname = '".$lastname."',
												business = '".$business."',
												phone = '".$phone."',
												address = '".$address."',
												city = '".$city."',
												state = '".$state."',
												zp = '".$zp."'";

						$fm = "SELECT email FROM user WHERE id = ".$id;

						$mq = $db->query($fm,true,true);

						if($mq["num_rows"] == 1){
							if($email != $mq["query"][0]["email"]){
								$upd .= ", email = '".$email."'";
							}
						}

						if(isset($_POST["img"]["dir_name"])){
							$upd .= ", img = '".$_POST["img"]["dir_name"]."'";
						}

						if(!empty($pass)){
							if(strlen($pass) >= 6){
								$upd .= ", pass = '".$lb->encrypt($pass)."'";
							}else{
								$lb->toClient(false,$callback,"ShortPass");
								return false;
							}
						}

						$upd .= " WHERE id =".$id;


						$qd = $db->query($upd,false,true);

						if($qd["status"]){
							$lb->toClient(true);
						}else{
							$lb->defaultQuery();
						}
					break;
					
					default:
						$lb->defaultRequest();
					break;
				}
			break;

			case 'PUT':
				//Post handled like PUT

			break;

			case 'DELETE':
				//Post handled like Delete

			break;
			
			default:
				$lb->defaultMethod($method);
			break;
		}

		$db->close($connection);

		//$db = new FriendofMySQL($execute);
	} 
	//print_r($_GET);
	//Classic HTTP method request hangling

	//Third FOM MySQL Server connection 
	

?>