const myServer = "";
var reload = new CustomEvent("ReloadEvent",{});
var vd = new VueDiana();

function IsValidJSONString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

var viewComponent = Vue.component('route-view',{
  template: `
    <div>
      <b-container v-if="pattern.length">
        <b-row>
          <b-col class="text-center">
            <b-img src="media/img/logo_d.png" fluid style="max-width: 182px;"></b-img>
          </b-col>
        </b-row>
      </b-container>

      <b-container v-else>
        <b-row>
          <b-col class="text-center">
            <b-img src="media/img/logo_d.png" fluid style="max-width: 182px;"></b-img>
          </b-col>
        </b-row>
        
        <b-row>
          <b-col offset=2 >
            <h4 style="color: #1E2446;" class="font-boldy elmessiri">{{ pattern.title }}</h4>
          </b-col>
        </b-row>
        
        <b-row>
          <b-col offset=2 sm=8 style="border-bottom: 1px solid black;" class="text-center"><span style="background-color: white; position: absolute; margin-left: -23px; margin-top: -23px; padding: 10px; color: #8F9093;">{{ pattern.width }}"</span></b-col>
        </b-row>
          
        <b-row style="padding-top: 20px;">
          <b-col offset=2 sm=8 class='text-center no-padding'>
            <b-img :src="pattern.img" fluid style="width: 100%;"></b-img>
          </b-col>
          <b-col sm=1 class="no-padding" style="border-right: 1px solid black; max-width: 20px;">
           
          </b-col>

          <b-col style=" margin-top: auto; margin-bottom: auto; ">
             <span class="mx-auto" style="color: #8F9093; background-color: white; position: absolute; padding-top: 10px; margin-left: -20px!important; margin-top: -25px!important; padding-bottom: 10px!important;">{{ pattern.height }}"</span>
          </b-col>
        </b-row>
        <b-row style="padding-top: 20px;">
          <b-col class="text-center" v-for="(item,index) in materials">
            <b-img :src="item.img" class="spec-material" fluid></b-img> <br>
            <span style="color: #737375;">{{ item.title }}</span>
          </b-col>
        </b-row>
      </b-container>
       
      <div class="custom-footer " style="width: 100%; position: fixed; bottom: 0; background-color: #002440;">
         <h4 style='text-align: center!important; color: #AAACB2;'>Urban City Designs. All rights reserved</h4>
      </div>
    </div>
  `,
  data(){
    return {
       title: "",
       pattern: [],
       materials: [],

       cutTypeOptions: [
        "Not cut type selected",
        "Water Jet",
        "Cut by hand",
        "Mix cut",
        "Nominal dimension",
        "Real dimension",
      ],

      finishOpt: [
        "Not specified",
        "Honed",
        "Polished",
        "Tumbled",
      ],
    }
  },
  beforeCreate(){
    NProgress.start();

    var main = this;
    //asset?r=collection&a=dmater&id=
    axios.get('asset?r=collection&a=dcone&id='+id).then(function(response){
      console.log(response)
      if(response.data.hasOwnProperty("info")){
        main.pattern = response.data.info[0];
      }else{
        main.title = "pattern not found";
      }
      
    }).catch(function(error){
      
      console.log(error);

    }).then(function(){
      axios({
          method: 'get',
          url: 'asset?r=dp&a=dpone&id='+main.pattern.dp,
      }).then(function (response) {
            //handle success
            console.log(response);
            console.log(response.data);

            let call = response.data.call;

            main.errors = [];

             switch(call){

               case true:
                 if(response.data.hasOwnProperty("info")){
                    var aux = response.data.info[0];

                    for(var key in aux) {
                       if(key != "img"){
                          main.pattern[key] = aux[key];
                       }
                      
                    }
                 }
               break;

               case false:
                 switch(response.data.errors){

                   default:
                     vd.toastError();
                   break;
                 }
               break;
               
               default:
                 vd.toastError();
               break;
             }
      }).catch(function (response) {
            //handle error
            console.log(response);
      }).then(function(){
        axios({
            method: 'get',
            url: 'asset?r=collection&a=dmater&id='+main.pattern.dp,
        }).then(function (response) {
              //handle success
              console.log(response);
              console.log(response.data);

              let call = response.data.call;

              main.errors = [];

               switch(call){

                 case true:
                   if(response.data.hasOwnProperty("info")){
                      main.materials = response.data.info;
                   }
                 break;

                 case false:
                   switch(response.data.errors){

                     default:
                       vd.toastError();
                     break;
                   }
                 break;
                 
                 default:
                   vd.toastError();
                 break;
               }
        }).catch(function (response) {
              //handle error
              console.log(response);
        }).then(function(){
          main.$forceUpdate();
        });
      });
    });
  },
  created(){
    var main = this;
    
  },
  mounted(){
    NProgress.done();
  },

});

new Vue({
  el: '#app',
  methods: {
    printPage(){
      window.print();
    },
    closeWindow(){
      window.close();
    }
  },
});