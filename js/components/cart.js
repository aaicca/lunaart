const CartComponent = Vue.component("cart-order",{
 	template: 
 	`<div>
 		<div v-if="container" class='cart-container'>
		 	<button @click="closeCart" class="minimize-button"><i class="far fa-window-minimize"></i></button>
	 		<b-container fluid>
				<b-row class='img-container'>
					<b-col class="text-center">
						<div v-viewer class="images clearfix">
						    <template>
						      <b-img :src="mainCartImg" fluid></b-img>
						    </template>
						</div>
						
						<span class="display-text text-left">{{ displayText }} <br> {{ displayMaterialsStr }}</span>

						<div class="action-container">

							<button type="button" @click="createSpecPage"><i class="fas fa-file-download"></i></button>
							<button type="button" @click="continueWithOrder"><i class="fas fa-arrow-right"></i></button>

						</div>
					</b-col>
				</b-row>

				<b-row class='parent-row'>

					<b-col sm=6>
						<span class="gold-flat-text">SELECTED COLLECTIONS</span>
						<b-col class='items-col'>
							<ul>
								<li v-for="(m,index) in $root.$data.cart.pattern">
									<button class="remove" @click="removeCollection(index)">X</button>
									<b-img :src="m.img" @click="setDisplay(m.img,m.text,m.materialsStr); $root.$data.cart.display.id = m.id" fluid></b-img>
								</li>
							</ul>
						</b-col>
					</b-col>

					<b-col sm=6>
						<span class="gold-flat-text">SELECTED MATERIALS</span>
						<b-col class='items-col'>
							<ul>
								<li v-for="(m,index) in $root.$data.cart.build">
									<button class="remove" @click="removeMaterial(index)">X</button>
									<b-img :src="m.img" @click="setDisplay(m.img,m.text)" fluid></b-img>
								</li>
							</ul>
						</b-col>
					</b-col>

				</b-row>
			</b-container>
		</div>

		<div class="cart-toggle" v-if="$root.$data.displayButton && !container">
			<button @click="openCart"><i class="far fa-window-maximize"></i></button>
		</div>
	</div>`,

 	data(){
		return {
			index: 0,
			count: 0,
			mainCartImg: null,
			container: false,
			displayText: null,
			displayMaterialsStr: null,
			displayImg: null,
			displayButton: false,
			imgContainer: null,
			title: "Lorem",
			images: [
				"media/img/asset.png",
				"media/img/asset-wide.png"
			],

			vd: new VueDiana(),
		}
	},
	created(){
		this.display = this.images[0];

	},
	mounted(){
		this.mainCartImg = this.$root.$data.cart.display.img;
	},
	methods: {
		createSpecPage(){
			if(this.$root.$data.cart.display.id != null){
				window.open("spec-page?pattern="+this.$root.$data.cart.display.id,"_self");
			}	
		},
		setDisplay(img,text,materialsStr = ""){
			this.mainCartImg = img;
			this.displayText = text;
			this.displayMaterialsStr = materialsStr;
		},
		continueWithOrder(){
			var main = this;
			main.container = false;
		
			if(!this.$root.$data.session.status){
				this.$root.$refs.loginModal.show();
			}else{

				NProgress.start();
				var submitForm = new FormData();

			    submitForm.append("request","add");
			    submitForm.append("method","post");

			    //Here apennds stringified all content
			    submitForm.append("cart",JSON.stringify(this.$root.$data.cart));

				axios.post('my-saves',submitForm,{
			        headers: {
			           'content-type': 'multipart/form-data'
			        }
			    }).then(function (response) {
			            //handle success
			            console.log(response);
			            console.log(response.data);

			            let call = response.data.call;

			            main.errors = [];

			             switch(call){

			               case true:
								main.$root.$data.cart = {
									display: {},
									pattern: [],
									build: [],
									details: {}
								};
								
								main.vd.toastSuccess();
								window.open("my-saves");
			               break;

			               case false:
			                 switch(response.data.errors){

							   default: 
								 main.vd.toastError();
							   break;
			                 }
			               break;
			               
			               default:
			                 main.vd.toastError();
			               break;
			             }
			    }).catch(function (response) {
			            //handle error
			            console.log(response);
			    }).then(function () {

				        NProgress.done();
				});
			}			

		},
		closeAtAll(){
			this.$root.$data.cart.pattern = []; 
			this.$root.$data.displayButton = false; 
			this.container = false;
		},
		removeMaterial(index){
			var aux = this.$root.$data.cart.build;

				aux.splice(index,1);
			
			this.$root.$data.build = aux;

		},
<<<<<<< HEAD
		removeCollection(index){
			var aux = this.$root.$data.cart.pattern;

				aux.splice(index,1);
			
			this.$root.$data.pattern = aux;
=======
		setImage(){
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7

		},
		closeCart(){
			this.container = false;
		},
		openCart(){
			this.container = !this.container;
		}
	}
});