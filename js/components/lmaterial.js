const lMaterialComponent = Vue.component("lmaterial",{
	//Side default is null, 1 equals to landing page and 2 is admin dashboard, 3 is client dashboard.
	props:{
		side: null
	},
	template: 
	`<div class='nav-padder'>
		
		<b-container fluid v-if=loading>
			<b-row v-if="materials.length">
				<b-col class="text-center padding">
					<l-spinner color="luna-text-gold"></l-spinner>
				</b-col>
			</b-row>
		</b-container>

		<b-container fluid v-else>
		
			<b-row>
	          <b-col>
	            <h4 style="color: #1E2446;" class="font-boldy elmessiri gold-flat-text component-title">Materials</h4>
	          </b-col>
	        </b-row>

			<b-container >

				<b-row>

					<b-input-group class="mt-3">
				       <b-form-input v-model="sorting" @input="searchMat" placeholder="SEARCH MATERIAL" autocomplete=off />
				    </b-input-group>

				</b-row>


			</b-container>

			<b-container v-if="sorting">
				
				<b-row>

					<b-col sm=4 md=2 v-for="m in sortingM" @click="setItem(m)">
						<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" img-top>
							<span class='material-title'>{{ m.title }}</span>
					    </b-card>
					</b-col>

				</b-row>


			</b-container>

			<b-container v-else>

				<b-tabs class="nav-justified material-tabs" content-class="mt-3" v-if="materials.length">
					<b-tab title="Stone" active>
						
						<b-container>
							<b-row>
								<b-col sm=4 md=2 v-for="m in stone" @click="setItem(m)">
									<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" img-top>
										<span class='material-title'>{{ m.title }}</span>
								    </b-card>
								</b-col>
							</b-row>
						</b-container>

					</b-tab>

					<b-tab title="Mirror">
						
						<b-container>	
							<b-row>
							
								<b-col sm=4 md=2 v-for="m in mirror" @click="setItem(m)" class="material-card">
									<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" img-top>
										<span class='material-title'>{{ m.title }}</span>
								    </b-card>
									
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Metal">
						
						<b-container>	
							<b-row>
							
								<b-col sm=4 md=2 v-for="m in metal" @click="setItem(m)" class="material-card">
									<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" img-top>
										<span class='material-title'>{{ m.title }}</span>
								    </b-card>
									
								</b-col>

							</b-row>

						</b-container>

					</b-tab>

					<b-tab title="Glass">
						
						<b-container>	
							<b-row>
							
								<b-col sm=4 md=2 v-for="m in glass" @click="setItem(m)" class="material-card">
									<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" img-top>
										<span class='material-title'>{{ m.title }}</span>
								    </b-card>
									
								</b-col>

							</b-row>
						</b-container>

					</b-tab>

					<b-tab title="Gemstones">

						<b-container>	
							<b-row>
							
								<b-col sm=4 md=2 v-for="m in gemstones" @click="setItem(m)" class="material-card">
									<b-card no-body :img-src="m.img" img-alt="Image"class="material-card" img-top>
										<span class='material-title'>{{ m.title }}</span>
								    </b-card>
									
								</b-col>

							</b-row>
						</b-container>
						
					</b-tab>
				</b-tabs>
				
				<b-row v-else>
					<b-col class="text-center">
						<p>There's no materials in your library</p>
					</b-col>
				</b-row>
			</b-container>

			<div v-viewer id="material-g" class="gallery-invisible images clearfix gallery-card">
			    <template>
			      <b-img fluid :src="mainImage" class="image" :key="mainImage"/>
			    </template>
			</div>

			<b-button v-if="addButton"  @click="addToCart" class="addToCartButton btn-gold">ADD THIS MATERIAL TO YOUR <br> SAVED ITEMS</b-button>
		 </b-container> 
	</div>
	  `,
	data() {
		return {
			materials: [],
			stone: [],
			mirror: [],
			metal: [],
			glass: [],
			gemstones: [],
			loading: false,
			sorting: "",
			sortingM: [],

			mainImage: "media/img/asset.png",

			mainMaterial: null,

			addButton: false
		}
	},
	mounted(){
		console.log(this.materials);
		console.log(this.sortingM);
		
		var main = this;
		setInterval(function(){ 
			try{
				var viewer = main.$el.querySelector('#material-g').$viewer
				if(!viewer.isShown){
					main.addButton = false;
				}
			}catch(e){}
			
		}, 3000);

				
	},
	created(){
		var main = this;
		main.loadMaterial();

		window.addEventListener("reloadMaterials",function(){
			main.loadMaterial();
			main.$forceUpdate();
		});
	},
	updated(){
	},
	methods: {
		setItem(material){
			console.log(material);
			
			var main = this;

			main.mainImage = material.img;
			main.mainMaterial = material;

			setTimeout(function(){
				main.show();
				main.addButton = true;
			},800);
			
		},
		addToCart(){
			console.log(this.$root.$data.cart.build);

			var newItem = this.mainMaterial;
			var array = this.$root.$data.cart.build;
			
			var flag = false;

			for(var o in array){
				if(array[o].id == newItem.id){
					flag = false;
					throw false;
				}else{
					flag = true;
				}
			}

			this.$root.$data.cart.build.push(this.mainMaterial);
		},
		show() {
			const instViewer = this.$el.querySelector('#material-g').$viewer
			instViewer.show();
			console.log(instViewer);
		},
		searchMat(){
			var main = this;
			
			this.sortingM = this.materials.filter(function(index) {
				console.log(index.title.toLowerCase())
				console.log(main.sorting.toLowerCase())
				if(index.title.toLowerCase().match(main.sorting.toLowerCase())){
					return index;
				}
				
			});

			console.warn(this.sortingM)
		},
		loadMaterial(){
			//asset?r=material&a=da
			 var main = this;

			 main.loading = true;

			 main.materials = [];

			 main.stone = [];
			 main.mirror = [];
			 main.metal = [];
			 main.glass = [];
			 main.gemstones = [];

			  axios({
		        method: 'get',
		        url: 'asset?r=material&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		          main.errors = [];

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  	main.materials = response.data.info;

		                  	main.sortingM = main.materials;

							for(var m in main.materials){

						      	main.materials[m].category = parseInt(main.materials[m].category);
						      	
						      	switch(main.materials[m].category){
							  		case 1:
							  			main.stone.push(main.materials[m]);
							  		break;

							  		case 2:
							  			main.mirror.push(main.materials[m]);
							  		break;

							  		case 3:
							  			main.metal.push(main.materials[m]);
							  		break;

							  		case 4:
							  			main.glass.push(main.materials[m]);
							  		break;

							  		case 5:
							  			main.gemstones.push(main.materials[m]);
							  		break;

							  		default:
							  			console.log("Nu")
							  		break;
							  	}
					        }
		               }else{
		                  main.materials = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }

		           main.loading = false;
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        main.loading = false;
		      });

		    main.$forceUpdate();
		},
	}
});
