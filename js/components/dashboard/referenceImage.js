const ReferenceImageFormComponent = Vue.component("image-component",{
	template: 
		`<div>
			<b-container fluid>
			
				<b-row>
					<b-col cols=12>
						<b-input-group v-b-modal.modal-prefile>
							<b-form-input type="text" v-model="fileName" placeholder="Select image file..."></b-form-input>
						
							<b-input-group-append>
								<b-button class="btn btn-gold">UPLOAD</b-button>
							</b-input-group-append>
						</b-input-group>
					</b-col>
				</b-row>

				<b-modal ref="modal-prefile" id="modal-prefile" centered hide-footer hide-header>
					<b-container fluid>
						<b-row>
							<b-col>
								<p class="text-center" style="text-transform: capitalize;">Add a file where each section is marked and numbered, in addition to a rule that serves as a reference of dimensions as shown in the following example</p>
							</b-col>
						</b-row>

						<b-row>
							<b-col cols=12 class="text-center">
								<b-img src="media/img/asset.png" fluid></b-img>
							</b-col>
						</b-row>

						<b-row>
							<b-col class="text-center">
								<br>
								<b-button class="btn btn-bordered" @click="understood">Understood</b-button>
							</b-col>
						</b-row>
					</b-container>
				</b-modal>

				<b-form-file type="file" @change="setThisFile" id="main-file" style="visibility: hidden; position: absolute; z-index: -999"></b-form-file>
			</b-container>
		</div>`,
	data() {
		return {
			fileName: ""
		}
	},
	created(){
	},
	update(){

	},
	methods: {
		setThisFile(event){
			let file = event.target.files[0];
			this.fileName = file.name;
			this.$emit("file-ref", file);
		},
		understood(){
			this.$refs['modal-prefile'].hide();
			let input = document.getElementById("main-file");
				input.click();
		},
	}
});