const MySavesComponent = Vue.component("my-saves",{
	template: 
		`<div>
			<b-container fluid>
				<div v-if="!orderReady">
					<b-row style="padding: 20px;">
						<b-row  class="full-width"><b-col class="underline-row" sm=12><h4>Patterns</h4></b-col></b-row>
						<b-row v-if="collections">
							<b-col cols=2 class="text-center" v-for="(o,i) in collections">
								<b-col xs=8 class="padding"><b-img fluid :src="o.img"></b-img></b-col>
								
								<!-- <b-button class="full-width mysave-button">CUSTOMIZE THIS PATTERN</b-button> -->
								<b-button class="full-width mysave-button" @click="openOrder(o)">REQUEST PRICE</b-button>
								<b-button v-if="$root.session.role == 2" class="full-width mysave-button">SUBMIT ORDER</b-button>
								<b-button @click="removeAsset(o.id)" class="full-width mysave-button">REMOVE</b-button>
							</b-col>
						</b-row>
	
						<b-row v-else>
							<b-col>
								<p>You don't have collections saved</p>
							</b-col>
						</b-row>
					</b-row>
	
					<b-row style="padding: 20px;">
						<b-row class="full-width"><b-col class="underline-row" sm=12><h4>Materials</h4></b-col></b-row>
						<b-row v-if="collections">
							<b-col xs=12 sm=2 lg=2 class="text-center" v-for="(o,i) in materials">
								<b-col xs=8 class="padding">
									<b-img fluid :src="o.img"></b-img> <br>
									<span class="mysave-material-title">{{ o.title }}</span>
								</b-col>
	
								<b-button @click="removeAsset(o.id)" class="full-width mysave-button">REMOVE</b-button>
							</b-col>
						</b-row>
	
						<b-row v-else>
							<b-col>
								<p>You don't have materials saved</p>
							</b-col>
						</b-row>
					</b-row>
				</div>
				
				<div v-else style="padding: 5px;">
					<button class="btn-flat padding" style="padding: 1%;" @click="orderReady = false"><i class="fas fa-chevron-left fa-2x"></i></button>
					<creating-order :item='orderItem' ></creating-order>
				</div>
			</b-container>
		</div>
		`,
	data() {
		return {
			mySaves: {},
			materials: {},
			collections: {},
			vd: new VueDiana(),

			orderReady: false,
			orderItem: {},
		}
	},
	created(){
		this.loadAssets();
	},
	update(){

	},
	methods: {
		openOrder(o){
			console.log(o);
			var main = this;
			NProgress.start();

			var urlBuild = 'my-saves';
			var materials = JSON.parse(o.materials);

			for (var i=0; i<materials.length; ++i) {
				if (urlBuild.indexOf('?') === -1) {
					urlBuild = urlBuild + '?materials[]=' + materials[i];  
				}else {
					urlBuild = urlBuild + '&materials[]=' + materials[i];
				}
			}

			console.log(urlBuild);
			urlBuild += "&g=oneSave&id="+o.asset;

			axios({
				method: 'get',
				url: urlBuild,
			}).then(function (response) {
				//handle success
				console.log(response);
				console.log(response.data);
		
				let call = response.data.call;
				main.mySaves = response.data.info;
		
					switch(call){
						case true:
							var asset = response.data.info.query;
								main.orderItem.pattern = asset;
								main.orderItem.mySaveId = o.id;
								main.orderItem.pattern.preview = o.img;

								main.orderItem.pattern.finish = {};
								for(let key in asset.materials){
									main.orderItem.pattern.finish[key] = null;
								}

								main.orderItem.details = {
									po: 0,
								}
								
								main.orderItem.build = asset.materials;

								console.warn(main.orderItem);
								main.orderReady = true;
						break;
			
						case false:
							switch(response.data.errors){
			
							default:
								main.vd.toastError();
							break;
							}
						break;
						
						default:
							main.vd.toastError();
						break;
					}
			}).catch(function (response) {
				//handle error
				console.log(response);
			}).then(function () {
				NProgress.done();
			});
		},
		loadAssets(){
			var main = this;
			axios({
				method: 'get',
				url: 'my-saves?g=selfSaves',
			}).then(function (response) {
				//handle success
				console.log(response);
				console.log(response.data);
		
				let call = response.data.call;
				main.mySaves = response.data.info;
		
					switch(call){
						case true:
							main.collections = main.mySaves.filter(function(save) {
								return save.type == 0;
							});

							main.materials = main.mySaves.filter(function(save) {
								return save.type == 1;
							});
						break;
			
						case false:
							switch(response.data.errors){
			
							default:
								main.vd.toastError();
							break;
							}
						break;
						
						default:
							main.vd.toastError();
						break;
					}
			}).catch(function (response) {
				//handle error
				console.log(response);
			}).then(function () {
				NProgress.done();
			});
		},
		removeAsset(id){
			if(confirm("Are you sure that you want delete this item?")){
			
				var main = this;
				NProgress.start();

				var uploadForm = new FormData();

				uploadForm.append("id",id);
				uploadForm.append("request","delete");
				uploadForm.append("method","post");

				axios.post('my-saves',uploadForm,{
					headers: {
						'content-type': 'multipart/form-data'
					}
				}).then(function (response) {
					//handle success
					console.log(response);
					console.log(response.data);

					let call = response.data.call;

					switch(call){
						case true:
							main.vd.toastSuccess();
							main.loadAssets();
						break;

						case false:
							switch(response.data.errors){

								default:
									main.vd.toastError();
								break;
							}
						break;
						
						default:
							main.vd.toastError();
						break;
					}
				}).catch(function (response) {
					//handle error
					console.log(response);
				}).then(function () {
					NProgress.done();
				});
			}
		}
	}
});