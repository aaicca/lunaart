
function loadImage(url) {
  /*
   * We are going to return a Promise which, when we .then
   * will give us an Image that should be fully loaded
   */
  return new Promise(function(resolve){
    /*
     * Create the image that we are going to use to
     * to hold the resource
     */
    const image = new Image();
    /*
     * The Image API deals in even listeners and callbacks
     * we attach a listener for the "load" event which fires
     * when the Image has finished the network request and
     * populated the Image with data
     */
    image.addEventListener('load', () => {
      /*
       * You have to manually tell the Promise that you are
       * done dealing with asynchronous stuff and you are ready
       * for it to give anything that attached a callback
       * through .then a realized value.  We do that by calling
       * resolve and passing it the realized value
       */
      resolve(image);
    });
    /*
     * Setting the Image.src is what starts the networking process
     * to populate an image.  After you set it, the browser fires
     * a request to get the resource.  We attached a load listener
     * which will be called once the request finishes and we have
     * image data
     */
    image.src = url;
  });
}

const dpComponent = Vue.component("dessign-pattern",{
	template: 
	`<div class="cs-container">	
		
		<div class='padding' v-if='orderReady'>
			<button class="btn-flat padding" style="padding: 1%;" @click="orderReady = false"><i class="fas fa-chevron-left fa-2x"></i></button>
		</div>

		<transition name="fade">
			<creating-order :item='orderItem' v-if='orderReady' :entryPoint="true" :requestType="requestType"></creating-order>
		</transition>
		
		<div v-if='!orderReady'>
			
			<b-container fluid v-if="viewPattern">

				<b-row>
					<b-col>
						<button type="button" @click="viewPattern = false;" class="flat"><i class="fas fa-chevron-left"></i></button>
					</b-col>
				</b-row>
				<displaydp step="3"></displaydp>
			</b-container>

			<b-container class="yourImage-container" fluid v-if="viewYourImage">

				<b-row>
					<b-col sm=2>
						<button type="button" @click="viewYourImage = false;" class="flat"><i class="fas fa-chevron-left"></i></button>
					</b-col>

					<b-col sm=10>
						<h4 class="component-title">Your Image</h4>
					</b-col>
				</b-row>

				<b-row>
					<b-col class="padding">
						<b-form-file v-on:change="setYourImage" placeholder="Place your image..." drop-placeholder="Drop file here..." accept="image/jpeg, image/png, image/jpg"></b-form-file>

					</b-col>
				</b-row>

				<b-row>
					<b-col md=12 lg=11>
						<canvas class="card-shadow" id="yourImageCanvas" style="width: 100%; height: 70vh;"></canvas>
					</b-col>

					<b-col md=12 lg=1>
						<b-row class="editor-buttons">

							<b-col sm="3" md="12" class="padding">
								<b-button @click="eraserCanv" variant="primary" class="circle-button"><i class="fas fa-eraser"></i></b-button>
							</b-col>

						</b-row>

					</b-col>
				</b-row>
				
				<b-row>
					<b-col sm=11 class="text-right">
						<b-button variant="primary" @click="submitYourImage">SAVE</b-button>
					</b-col>
				</b-row>
				<div class="padding"></div>				
			</b-container>

			<b-container class='container-dp' fluid v-if="!viewYourImage && !viewPattern">
				<b-row>
					<b-col sm=12 md=6>
						<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
					</b-col>
				</b-row>

				<b-row>
					<b-col sm=4 class="text-left">
						<button type="button" @click="viewPattern = true" class="blue-link">SELECT PATTERN <i class="fas fa-caret-right"></i></button>
					</b-col>
					
					<!-- 
						<b-col sm=3 class="text-left">
							<button type="button" @click="viewYourImage = true" class="blue-link"> YOUR IMAGE <i class="fas fa-caret-right"></i></button>
						</b-col>
					-->
				</b-row>

				<b-row>
					<b-col sm="12" md="6" lg="5">
						<h5>{{ mainPattern.title }}</h5>
						
						<div class="text-center">
							<canvas id='designing' width='300px' height='300px' class="displayPattern "></canvas>
							<br><small>This is a sample preview</small>
							<p class="text-justify">{{ mainPattern.description }}</p>
						</div>
					</b-col>

					<b-col sm="12" md="6" lg="3">
						<b-form-group :label="'Section '+c" v-for="c in mainPattern.chromas">
							<b-input-group>
								<b-input-group-prepend>
									<b-img-lazy :src="setImg(preppendMaterial[(c-1)])" style="border: 2px solid #D8D9DD; height: 38px; width: auto" alt="" />
								</b-input-group-prepend>
								<b-form-select @input="setTexture($event,c)" :options="materials" />

								<b-form-select @input="addFinish($event,c)" :options="finishOpt" />
							</b-input-group>
						</b-form-group>
					
					</b-col>

					<b-col sm="12" lg="4" v-if="mainPattern.id != undefined">
						<b-row>

							<b-col sm="12">
								<b-form-group label="Design specs">
									<b-card no-body class="text-center">
										<div class="grey-luna input-height" style="padding-top: 5px;">
											{{ mainPattern.width }}" x {{ mainPattern.height }}" P/Sheet
										</div>
									</b-card>
								</b-form-group>
							</b-col>

							<b-col sm="12">
								<b-form-group label="1 sheet covers">
									<b-card no-body class="text-center">
										<div class="grey-luna input-height" style="padding-top: 5px;">
											{{ sf }} /SF
										</div>
									</b-card>
								</b-form-group>
							</b-col>

							<b-col sm="12">
								<b-form-group label="1 sheet covers">
									<b-card no-body class="text-center ">
										<div class="grey-luna input-height" style="padding-top: 5px;">
											$ {{ (parseFloat(mainPattern.price)).toFixed(2) }} P/Sheet
										</div>
									</b-card>
								</b-form-group>
							</b-col>
						
						</b-row>

						<b-row>

							<b-col sm=6 style="padding-right: 0px;">
								<b-form-group label="Sheets: " v-if="focusQ" >
									<b-input-group @mouseleave="focusQ = false" >
										<b-form-input type="number" @input="setS($event)" :value="quantity"  step="1"></b-form-input>
									</b-input-group>

								</b-form-group>
								
								<b-form-group label="Sheets: " v-else>
									<b-card no-body @mouseover="focusQ = true; focusSF = false" class="text-center ">
										
										<div class="grey-luna" style="height: 38px; padding-top:5px;">
								
											{{ quantity }}
										</div>
									</b-card>
								</b-form-group>
							</b-col>
							
							<b-col sm=6 style="padding-right: 0px;">
								<b-form-group label="SF covered: " v-if="focusSF" >
									<b-input-group @mouseleave="focusSF = false" >
										<b-form-input type="number" @input="setSFC($event)" :value="sfCovered"  step="1"></b-form-input>
									</b-input-group>

									
								</b-form-group>
								
								<b-form-group label="SF covered: "  v-else>
									<b-card no-body @mouseover="focusSF = true; focusQ = false" class="text-center ">
										
										<div class="grey-luna " style="height: 38px; padding-top:5px;">
											
											<span>{{ sfCovered }}</span>
										</div>
									</b-card>
								</b-form-group>
							</b-col>
							
							<b-col sm=12>
								<b-form-group label="PO price: ">
									<b-card no-body class="text-center ">
										<div class="grey-luna" style="height: 38px; padding-top:5px;">
											$ {{ po }}
										</div>
									</b-card>
								</b-form-group>
							</b-col>
						</b-row>
					</b-col>

				</b-row>

				<b-row v-if="mainPattern.id != undefined" >
					<b-col sm="3"  v-if="mainPattern.id != undefined">
						<b-form-group label="Design specs">
							<img class="img-fluid image" style="border: 7px solid #EDEEF0" v-for="(image, i) in images" :src="image" :key="i" @click="index = i">
							<vue-gallery-slideshow :images="images" :index="index" @close="index = null"></vue-gallery-slideshow>
						</b-form-group>
					</b-col>

					<b-col sm="5">
						<b-form-group label="Special notes: ">
							<b-form-textarea
								v-model="orderItem.extraMessage"
								placeholder="Detail us something..."
								rows="3"
							></b-form-textarea>
						</b-form-group>
					</b-col>
					<b-col sm="4" md="4">
						<b-row>

						    <b-col sm=12 class="text-right">
								<b-form-group
									label-cols-sm="3"
									label="Promo code:&nbsp;"
									label-align-sm="right"
									label-for="inputing"
									class="inline"
								>
								<b-form-input v-model="promoCode" id="inputing"></b-form-input>
								</b-form-group>
						    </b-col>

						    <b-col sm=12 class="text-right">
								<button class="btn-gold white-text text-white" block @click="orderReady = true; addToOrder">CONTINUE <i class="fas fa-angle-right"></i></button>
						    </b-col>
						</b-row>
					</b-col>
				</b-row>

				<img src="" id="rawImgChroma"/>

				<img src="" id="end"/>
					
				<div id="toWorkTexture">
					<img src="" id="toGreen" class="bluildChroma" />
					<img src="" id="toBlue" class="bluildChroma" />
					<img src="" id="toRed" class="bluildChroma" />
					<img src="" id="toPurple" class="bluildChroma" />
					<img src="" id="toCyan" class="bluildChroma" />
				</div>
			</b-container>

		</div>
	</div>`,/*cc: [
				{text: 'CHROMAS QUANTITY', value: null },
				{text: "1 Green  (#3cff00)",value: 1},
				{text: "2 Blue   (#0024ff)",value: 2},
				{text: "3 Red    (#ff0000)",value: 3},
				{text: "4 Purple (#ff00fd)",value: 4},
				{text: "5 Cyan   (#00fff6)",value: 5}

				data-chroma-key="#3cff00"
data-chroma-key="#0024ff"
data-chroma-key="#ff0000"
data-chroma-key="#ff00fc"
data-chroma-key="#00fff6"
			],*/
	data() {
		return {

			seen: true,
			message: "hola",

			viewPattern: false,
			viewYourImage: false,
			mainPattern: {},
			materials: [],

			build: [],

			colors: [],

			finish: [
				{value: null, text: ""},
				{value: null, text: ""},
				{value: null, text: ""},
				{value: null, text: ""},
				{value: null, text: ""},
			],

			focusQ: false,
			focusSF: false,

			quantity: 0,
			sfCovered: 0,
			po: "0.00",
			promoCode: "",
			sf: 0,

			finishOpt: [
				{value: 1, text: "Honed"},
				{value: 2, text: "Polished"},
				{value: 2, text: "Tumbled"},
			],

			loading: false,
			errors: [],

			images: [],
			index: null,

			preppendMaterial: ["media/img/asset.png",
							   "media/img/asset.png",
							   "media/img/asset.png",
							   "media/img/asset.png",
							   "media/img/asset.png"],

			orderReady: false,

			orderItem: {
				extraMessage: null,
			},

			yourImageRequest: {
				original: null,
				canvas: null
			},

			vd: new VueDiana(),

			requestType: null,
		}
	},
	created(){
		this.loadMaterial();

		this.requestType = (this.$root.$data.session.role == 3 ? false : true );

		this.$root.$data.componentTitle = "Pattern Customization";
		var main = this;
		window.addEventListener("closeDisplayPattern",function(){
			main.viewPattern = false;
		});

	},
	mounted(){
		console.log(this.$root.$data.mainPattern);
		var main = this;

		var ctx = document.getElementById('designing').getContext('2d');

		this.mainPattern = {
			title: "No selected pattern",
			img: "media/img/asset.png"

		}

		this.$root.$data.mainPattern = this.mainPattern;

		var drawIterval = setInterval(function(){
			if(!main.orderReady){
				try{
					main.mountManiCanvas();
					var canvas = document.getElementById("designing");
					main.orderItem["pattern"].preview = canvas.toDataURL();
				}catch(e){}
				
			}
		},1500);
	},
	updated(){
		if(this.$root.$data.mainPattern.hasOwnProperty("id")){
			if(this.mainPattern.id != this.$root.$data.mainPattern.id){
				this.restartDisplay();

				if(this.mainPattern.hasOwnProperty("id")){
					this.mainPattern.chromas = parseInt(this.mainPattern.chromas);

					this.addToOrder();

					this.setDisplay(this.mainPattern.img);
				}
			}else{
				this.addToOrder();
				this.setDisplay(this.mainPattern.img);
			}
			console.log(this.$root.$data.mainPattern);
		}else{
			console.log("Nope");
		}
		
	},
	methods: {
		setYourImage(e){
			var main = this;
			try{
				this.yourImageRequest.original = e.target.files[0];

				var img = new Image();
					img.onload = function(){
						main.drawImageScaled(img);
					};
					img.src = URL.createObjectURL(this.yourImageRequest.original);

			}catch(ex){
				console.log(ex)
			}
	      
	    },
	    eraserCanv(e){
	    	console.log(e);
	    	var main = this;
	    	var img = new Image();
				img.onload = function(){
					main.drawImageScaled(img);
				};
				img.src = URL.createObjectURL(this.yourImageRequest.original);
	    },
		drawImageScaled(img) {
			var main = this;
			var canvas = document.getElementById('yourImageCanvas');
			var signaturePad = new SignaturePad(canvas,{
				onEnd: function(e){
					main.yourImageRequest.canvas = canvas.toDataURL();
				}
			});
				signaturePad.minWidth = 20;
				signaturePad.maxWidth = 20;
				signaturePad.penColor = "rgba(193, 66, 66,0.2)";
			var ctx = canvas.getContext('2d');
			var canvas = ctx.canvas ;
			var hRatio = canvas.width  / img.width    ;
			var vRatio = canvas.height / img.height  ;
			var ratio  = Math.min ( hRatio, vRatio );
			var centerShift_x = ( canvas.width - img.width*ratio ) / 2;
			var centerShift_y = ( canvas.height - img.height*ratio ) / 2;  
				ctx.clearRect(0,0,canvas.width, canvas.height);
				if(Math.max(document.documentElement.clientWidth, window.innerWidth) <= 992){
					ctx.drawImage(img, 0,0, img.width, img.height,0,0,img.width*ratio, img.height*ratio);  
				}else{
					ctx.drawImage(img, 0,0, img.width, img.height,centerShift_x,centerShift_y,img.width*ratio, img.height*ratio);
				}
				

			window.addEventListener("resize", function(e){
				var canvas = document.getElementById('yourImageCanvas');
				var ratio =  Math.max(window.devicePixelRatio || 1, 1);
				    canvas.width = canvas.offsetWidth * ratio;
				    canvas.height = canvas.offsetHeight * ratio;
				    canvas.getContext("2d").scale(ratio, ratio);
				    signaturePad.clear();
				var ctx = canvas.getContext('2d');
				var canvas = ctx.canvas ;
				var hRatio = canvas.width  / img.width    ;
				var vRatio =  canvas.height / img.height  ;
				var ratio  = Math.min ( hRatio, vRatio );
				var centerShift_x = ( canvas.width - img.width*ratio ) / 2;
				var centerShift_y = ( canvas.height - img.height*ratio ) / 2;  
					ctx.clearRect(0,0,canvas.width, canvas.height);
				    if(Math.max(document.documentElement.clientWidth, window.innerWidth) <= 992){
						ctx.drawImage(img, 0,0, img.width, img.height,0,0,img.width*ratio, img.height*ratio);  
					}else{
						ctx.drawImage(img, 0,0, img.width, img.height,centerShift_x,centerShift_y,img.width*ratio, img.height*ratio);
					}
			});

			window.dispatchEvent(new Event('resize'));

		},
		submitYourImage(){
			NProgress.start();
			var main = this;
			var uploadForm = new FormData();

			main.yourImageRequest.canvas = main.vd.dataURLtoFile(main.yourImageRequest.canvas,"canvas.jpg");
			NProgress.done();
		},
		setImg(src){
			return src;
		},
		addToOrder(e){

			this.orderItem["pattern"] = this.mainPattern;

			this.orderItem["pattern"].finish = this.finish;
			this.orderItem["build"] = this.build;

			this.orderItem["materials"] = this.materials;

			this.orderItem["details"] = this.orderItem["details"] = {
				quantity: this.quantity,
				sfCovered: this.sfCovered,
				po: this.po,
				sf: this.sf,
				promoCode: this.promoCode
			};

			this.orderItem["yourimage"] = this.yourImageRequest;
			
			console.log(this.orderItem);
		},
		addFinish(e,i){
			this.finish[(i-1)] = this.finishOpt[(e-1)];
		},
		setS(q){
			console.log(q);

			this.sfCovered = (q*this.sf).toFixed(4);
			this.quantity = q;

			this.setPO();

			console.log(this.quantity);
			console.log(this.sfCovered);

			//this.po = q*(parseFloat(this.mainPattern.price)).toFixed(2);

		},
		setSFC(s){
			
			console.log(s);

			var aux = Math.ceil(s/this.sf);

			this.quantity = aux;
			this.sfCovered = s;

			this.setPO();

			console.log(this.quantity);
			console.log(this.sfCovered);
		},
		setPO(){
			this.po = (this.quantity*this.mainPattern.price).toFixed(2);


			this.orderItem["details"] = {
				quantity: this.quantity,
				sfCovered: this.sfCovered,
				po: this.po,
				sf: this.sf,
				promoCode: this.promoCode
			}
		},
		mountManiCanvas(){
			var main = this;
			var flag = false;

			var idImgs = ["toGreen",
						  "toBlue",
						  "toRed",
						  "toPurple",
						  "toCyan"];

			this.colors.every(function(element){
				if(element !== null){

					flag = true;
				}else{	
					return false;
				}
			});

			try{
				if(flag){
					for(var img in main.colors){

						var designing = document.getElementById("designing"),
							ctx = designing.getContext('2d');

						ctx.drawImage(document.getElementById(idImgs[img]),0,0,designing.width,designing.height);
					}
				}
			}catch(e){
				console.log(e)
			}
			

			console.log(flag);
		},
		restartDisplay(){
			var ctx = document.getElementById('designing').getContext('2d');

				ctx.clearRect(0, 0, 300, 300);

				this.mainPattern = this.$root.$data.mainPattern;

				this.build = [];
				this.colors = [];
				this.finish = [];
				this.orderItem = {};

				this.preppendMaterial = ["media/img/asset.png",
										   "media/img/asset.png",
										   "media/img/asset.png",
										   "media/img/asset.png",
										   "media/img/asset.png"];

				this.images = [this.mainPattern.imgDetail];// Optional
				
				this.sf = 0;

				this.sf = (parseFloat(this.mainPattern.width)*parseFloat(this.mainPattern.height))/144;

				this.sf = this.sf.toFixed(4);
		},
		setDisplay(src){
			try{
				var ctx = document.getElementById('designing').getContext('2d');

				var newPT = new Image(); 

					newPT.onload = function(){
						ctx.drawImage(newPT,0,0,300,300);
					}

				newPT.src = src;
			}catch(e){
				console.log(e)
			}
			
		},
		setTexture(e,i){
			/* A better name to this function should processing the first step to build colors object */
			var main = this;

			/* Pairing color with material */
			

			var selectedMaterial = this.materials.filter(function (index) {
				return index.id === e;
			});

			this.build[(i-1)] = selectedMaterial[0];

			this.preppendMaterial[(i-1)] = selectedMaterial[0].img;

			console.log(this.build);

			var tempBuild = this.build;

			/* Setting up the first step of replacing all color even if theres some of them left */

			var stepping = "";

			loadImage(this.mainPattern.imgChroma).then(function(houseImage){
			  console.log(houseImage);

			  for(var e = 0; e <= (main.mainPattern.chromas-1); e++){
			  	if(e == 0){
						main.colors[e] = main.removeChroma(e,houseImage,( e in main.build  ? main.build[e].img : null));
			  	}else if(e == 1){
			  		main.colors[e] = main.removeChroma(e,houseImage,( e in main.build  ? main.build[e].img : null));
			  	}else if(e == 2){
			  		main.colors[e] = main.removeChroma(e,houseImage,( e in main.build  ? main.build[e].img : null));
			  	}else if(e == 3){
			  		main.colors[e] = main.removeChroma(e,houseImage,( e in main.build  ? main.build[e].img : null));
			  	}else if(e == 4){
			  		main.colors[e] = main.removeChroma(e,houseImage,( e in main.build  ? main.build[e].img : null));
			  	}
			  }


			  main.$forceUpdate();

			 // main.removeChroma(e,chromaPattern)
			});

		},
		removeChroma(step,imgChroma,textureURL = "media/img/white.jpg"){

			console.log(arguments);

			var nr,ng,nb, target, color;
				color = step;

			var main = this;

			if(typeof textureURL == undefined || textureURL == null){
				textureURL = "media/img/white.jpg";
			}

			switch(color){
				case "3cff00":
				case "green":
				case 0:

					nr = 60;
					ng = 255;
					nb = 0;

					target = "toGreen";

					console.warn(target)
					console.warn(color)
				break;

				case "0024ff":
				case "blue":
				case 1:

					nr = 0;
					ng = 36;
					nb = 255;

					target = "toBlue";

					console.warn(target)
					console.warn(color)
				break;

				case "ff0000":
				case "red":
				case 2:

					nr = 255;
					ng = 0;
					nb = 0;

					target = "toRed";

					console.warn(target)
					console.warn(color)

				break;

				case "ff00fd":
				case "purple":
				case 3:

					nr = 255;
					ng = 0;
					nb = 253;

					target = "toPurple";

					console.warn(target)
					console.warn(color)

				break;

				case "00fff6":
				case "cyan":
				case 4:

					nr = 0;
					ng = 255;
					nb = 246;

					target = "toCyan";

					console.warn(target)
					console.warn(color)

				break;

				default:
					nr = 255;
					ng = 255;
					nb = 255;

					console.warn("White")

				break;
			}

			var flag = "";

			var texture = new Image();
				texture.src = textureURL;

				console.log(textureURL);

				texture.onload = function(){
					var temp = document.createElement("canvas");
					var textureCanvas = document.createElement("canvas");

						temp.id = "tempCanvas";
						temp.width = 550;
						temp.height = 550;
						temp.style.border = "1px solid";

						textureCanvas.id = "textureCanvas";
						textureCanvas.width = 550;
						textureCanvas.height = 550;
						textureCanvas.style.border = "1px solid";


					var body = document.getElementsByTagName("body")[0];
						body.appendChild(temp);

						body.appendChild(textureCanvas);


					var canvas = document.getElementById("tempCanvas"),
						ctx = canvas.getContext("2d");

						ctx.drawImage(imgChroma, 0, 0,550,550);

						console.log(imgChroma);

					var imgd = ctx.getImageData(0, 0, 550, 550),
						
						pix = imgd.data,
						newColor = { r:0, g:0, b:0, a:0}; // RGBA transparent

						for(var i = 0, n = pix.length; i <n; i += 4) {
							
							var r = pix[i],
						        g = pix[i+1],
						        b = pix[i+2];

						    if(r == nr && g == ng && b == nb){ 
						        // Change the color to the transparence.
						        pix[i] = newColor.r;
						        pix[i+1] = newColor.g;
						        pix[i+2] = newColor.b;
						        pix[i+3] = newColor.a;
						    }
						}

						console.log(target);

					var tempTextureCanvas = document.getElementById("textureCanvas"),
						textureCtx = tempTextureCanvas.getContext("2d");


					ctx.putImageData(imgd, 0, 0);

					textureCtx.drawImage(texture, 0, 0, 550, 550);
					textureCtx.drawImage(canvas, 0, 0, 550,550, 0,0,550,550);

					imgd = textureCtx.getImageData(0, 0, 550, 550),
						
						pix = imgd.data,
						newColor = { r:0, g:0, b:0, a:0}; // RGBA transparent

						for(var i = 0, n = pix.length; i <n; i += 4) {
							
							var r = pix[i],
						        g = pix[i+1],
						        b = pix[i+2];

						    if(r == 60 && g == 255 && b == 0){ 
						        // Change the color to the transparence.
						        pix[i] = newColor.r;
						        pix[i+1] = newColor.g;
						        pix[i+2] = newColor.b;
						        pix[i+3] = newColor.a;
						    }

						    if(r == 0 && g == 36 && b == 255){ 
						        // Change the color to the transparence.
						        pix[i] = newColor.r;
						        pix[i+1] = newColor.g;
						        pix[i+2] = newColor.b;
						        pix[i+3] = newColor.a;
						    }

						    if(r == 255 && g == 0 && b == 0){ 
						        // Change the color to the transparence.
						        pix[i] = newColor.r;
						        pix[i+1] = newColor.g;
						        pix[i+2] = newColor.b;
						        pix[i+3] = newColor.a;
						    }

						    if(r == 255 && g == 0 && b == 253){ 
						        // Change the color to the transparence.
						        pix[i] = newColor.r;
						        pix[i+1] = newColor.g;
						        pix[i+2] = newColor.b;
						        pix[i+3] = newColor.a;
						    }

						    if(r == 0 && g == 255 && b == 246){ 
						        // Change the color to the transparence.
						        pix[i] = newColor.r;
						        pix[i+1] = newColor.g;
						        pix[i+2] = newColor.b;
						        pix[i+3] = newColor.a;
						    }
						}

					textureCtx.putImageData(imgd, 0, 0);

					var display = document.getElementById(target);
						display.src = tempTextureCanvas.toDataURL();

						main.colors[step] = tempTextureCanvas.toDataURL();

						canvas.parentNode.removeChild(canvas);
						tempTextureCanvas.parentNode.removeChild(tempTextureCanvas);

					return flag;
				}

		},
		loadMaterial(){
			//asset?r=material&a=da
			 var main = this;

			 main.loading = true;

			 main.materials = [];

			  axios({
		        method: 'get',
		        url: 'asset?r=material&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		          main.errors = [];

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  	main.materials = response.data.info;

		               }else{
		                  main.materials = [];
		               }
		              
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }

		           main.loading = false;
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        main.loading = false;
		      });

		    main.$forceUpdate();
		}
	},
});