const SearchUserComponent = Vue.component("search-user",{
	template: 
	`<b-input-group class="mt-3">
	    <b-form-input v-model="search" @input="searchUser" placeholder="SEARCH USER" autocomplete="off" />
	    <b-input-group-append>
			<b-dropdown :text="dText" variant="none" slot="append">
				<b-dropdown-item @click="filterS('a')">All</b-dropdown-item>
				<b-dropdown-item @click="filterS('c')">Clients</b-dropdown-item>
				<b-dropdown-item @click="filterS('e')">Employees</b-dropdown-item>
			</b-dropdown>
			<!-- <button class="btn-gold" @click="searchUser">Button</button> -->
	    </b-input-group-append>
	  </b-input-group>`,
	data() {
		return {
			search: null,
			waiting: null,
			filter: null,
			dText: "All",
		}
	},
	created(){
	},
	update(){

	},
	methods: {
		searchUser(e){
			clearTimeout(this.waiting);
			var main = this;

			this.waiting = setTimeout(function () {
				console.log('Input Value:', main.search);
				main.$root.$emit("searchu",{par: main.search, arg: main.filter});
			}, 100);
		},
		filterS(str){
			switch(str){
				case 'a':

					this.dText = "All";
				break;

				case 'c':
					this.dText = "Clients";
				break;

				case 'e':
					this.dText = "Employee";
				break;

				default:
					this.dText = "All";
				break;
			}

			this.filter = str;
		}
	}
});