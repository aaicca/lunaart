const CompanyRequestComponent = Vue.component("company-request",{
		template: 
			`<div>
				<div v-if="info">
					<b-container fluid>
						<b-row style="padding-top: 5px;">
							<b-col>
								<button class="btn-flat padding" style="padding: 2%;" @click="info = false"><i class="fas fa-chevron-left fa-2x"></i></button>
							</b-col>

							<b-col>
								<button class="btn btn-blue padding" style="padding: 2%;"  v-b-modal.modal-pass>CONTINUE WITH APPROVAL</button>

								<b-modal id="modal-pass" title="Set the account password" centered hide-footer>
									<b-container>
										<b-row>
											<b-col sm=12>
												<b-form-group
													label="Set password"
												>
													<b-form-input v-model="approveInfo.password" trim></b-form-input>
												</b-form-group>
											</b-col>

											<b-col sm=12>
												<b-form-group
													label="Confirm the password"
												>
													<b-form-input v-model="approveInfo.passwordConfirm" trim></b-form-input>
												</b-form-group>
											</b-col>
										</b-row>
										
										<b-row>
											<b-col><b-button class="btn btn-flat" @click="generatePass">Generate password</b-button></b-col>
											<b-col class="text-right"><b-button class="btn btn-blue" @click="approveSend">APPROVE</b-button></b-col>
										</b-row>
									</b-container>
								</b-modal>
							</b-col>

							<b-col>
								<button class="btn btn-flat padding" @click="deleteU">
									<i class="material-icons">
										delete
									</i>
								</button>
							</b-col>
						</b-row>

						<b-row>
							<b-col sm=12><h5>Company information</h5></b-col>
						</b-row>

						<b-row>
							<b-col>
								<span class="request-label">Legal Company Name: </span> {{ request.companyName }}
							</b-col>

							<b-col sm=4>
								<span class="request-label">D.B.A.</span> {{ request.dba }}
							</b-col>
						</b-row>
						
						<b-row>
							<b-col><span class="request-label"> Business address: </span> {{ request.address }}</b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> City: </span> {{ request.ccity }}</b-col>
							<b-col><span class="request-label"> State: </span> {{ request.cstate }}</b-col>
							<b-col><span class="request-label"> Zip code:  </span> {{ request.czp }}</b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> Phone: </span> {{ request.cphone }}</b-col>
							<b-col><span class="request-label"> Fax: </span> {{ request.fax }}</b-col>
							<b-col><span class="request-label"> Website: </span> {{ request.website }}</b-col>
						</b-row>

						<b-row>
							<b-col sm=12><h5>Shipping Information</h5></b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> Shipping Address: </span> {{ request.saddress }}</b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> City: </span> {{ request.scity }}</b-col>
							<b-col><span class="request-label"> State: </span> {{ request.scity }}</b-col>
							<b-col><span class="request-label"> Zip code: </span> {{ request.szp }}</b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> Contact: </span> {{ request.scontact }}</b-col>
							<b-col><span class="request-label"> Phone: </span> {{ request.sphone }}</b-col>
						</b-row>

						<b-row>
							<b-col><h5>Contact Information</h5></b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> Main Contact/Owner: </span> {{ request.name+' '+request.lastname }}</b-col>
							<b-col><span class="request-label"> Title </span> {{ request.title }}</b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> Phone: </span> {{ request.phone }}</b-col>
							<b-col><span class="request-label"> E-mail: </span> {{ request.email }}</b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> Accounting contact: </span> {{ request.accountingContact }}</b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> Phone: </span> {{ request.accountingPhone }}</b-col>
							<b-col><span class="request-label"> Email: </span> {{ request.accountingEmail }}</b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> Authorized sales staff: </span> {{ request.authorizedSalesStaff }}</b-col>
						</b-row>

						<b-row>
							<b-col sm=12>
								<div style="border-bottom: 2px solid #B4956A; width: 100%;"></div>
							</b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> Printed name: </span> {{ request.printedName }}</b-col>
							<b-col><span class="request-label"> Title: </span> {{ request.title }}</b-col>
						</b-row>

						<b-row>
							<b-col><span class="request-label"> Date: </span> {{ request.dated }}</b-col>
							<b-col class="text-center">
								<b-img fluid :src="request.signature" style="border: 2px solid #B4956A; border-radius: 4px;"></b-img> <br>
								<small>Signature</small>
							</b-col>	
						</b-row>


					</b-container>
				</div>
				
				<div v-else>
					<b-container fluid>
						<b-row>
							<b-col sm=12 v-if="users">
								<b-list-group  style="padding-top:1%">
									<b-list-group-item href="#" v-for="u in users" @click="openRequest(u)" class="flex-column align-items-start" :class="(u.role < 2 ? 'luna-gold luna-goldHover text-white' : '')">
									<div class="d-flex w-100 justify-content-between">
										<h6 class="mb-1">{{ u.name }} {{ u.lastname }}</h6>
										<small class="text-muted">{{ u.fdated }}</small>
									</div>
	
									<p class="mb-1">
										{{ u.email }} <br>
										{{ u.phone }} <br>
	
									</p>
	
									<small class="text-muted">{{ u.business }}</small>
									</b-list-group-item>
								</b-list-group>
							</b-col>

							<b-col class="text-center" v-else style="padding-top: 15px">
								<span class="grey-text">We have no company requests</span>
							</b-col>
						</b-row>
					</b-container>
				</div>
				
			</div>
			`,
	data() {
		return {
			info: false,
			vd: new VueDiana(),
			users: null,
			request: null,

			approveInfo: {
				password: null,
				passwordConfirm: null
			}
		}
	},
	created(){
		this.loadRequests();
	},
	update(){

	},
	methods: {
		generatePass(){
			let temp = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
			this.approveInfo = {
				password: temp,
				passwordConfirm: temp
			}
		},
		approveUser(){
			console.log("Hi")
		},
		openRequest(u){
			var main = this;

			main.request = u;
			main.info = true;
		},
		approveSend(){
			var main = this;
			NProgress.start();

			var uploadForm = new FormData();

			for(var input in main.approveInfo){
				uploadForm.append(input,main.approveInfo[input]);
			}
			
			uploadForm.append("id",main.request.id);
			uploadForm.append("request","approve");
			uploadForm.append("method","post");

			axios.post('user',uploadForm,{
				headers: {
				'content-type': 'multipart/form-data'
				}
			}).then(function (response) {
					//handle success
					console.log(response);
					console.log(response.data);

					let call = response.data.call;

					switch(call){

					case true:
						main.vd.toastSuccess();
						main.loadRequests();
						main.info = false;
					break;

					case false:
						switch(response.data.errors){
							case "NotPassMatch":
								main.vd.toastError("","The choosen passwords doesn't match");
							break;

							case "NotEnoughForm":
								main.vd.toastError("","Give both passwords please");
							break;

						default:
							main.vd.toastError();
						break;
						}
					break;
					
					default:
						main.vd.toastError();
					break;
					}
				}).catch(function (response) {
					//handle error
					console.log(response);
				}).then(function () {
					NProgress.done();
				});
		},
		deleteU(){
			var main = this;
			var updateForm =  new FormData();
			var vd = new VueDiana();
	 
			if(confirm("Are you sure that you want delete this user?")){
			  updateForm.append("id",this.request.id);
			  updateForm.append("request","delete");
			  updateForm.append("method","post");
	 
			 axios.post('user',updateForm,{
			   headers: {
				  'content-type': 'multipart/form-data'
			   }
			 }).then(function (response) {
				   //handle success
				   console.log(response);
				   console.log(response.data);
	 
				   let call = response.data.call;
	 
				   main.errors = [];
	 
					switch(call){
	 
					  case true:
							main.vd.toastSuccess();
							main.loadRequests();
							main.info = false;
					  break;
	 
					  case false:
						switch(response.data.errors){
	 
						  default:
							vd.toastError();
						  break;
						}
					  break;
					  
					  default:
						vd.toastError();
					  break;
					}
			   }).catch(function (response) {
				   //handle error
				   console.log(response);
			   }).then(function () {
					NProgress.done();
			   });
			}
		 },
		loadRequests(){
			NProgress.start();
			var main = this;
			main.loading = true;
		
			axios({
				method: 'get',
				url: 'user?g=companyRequest',
			}).then(function (response) {
				//handle success
				console.log(response);
				console.log(response.data);
		
				let call = response.data.call;
		
				main.errors = [];
		
					switch(call){
		
					case true:
						main.users = response.data.info;
					break;
		
					case false:
						switch(response.data.errors){
		
						default:
							main.vd.toastError();
						break;
						}
					break;
					
					default:
						main.vd.toastError();
					break;
					}
			}).catch(function (response) {
				//handle error
				console.log(response);
			}).then(function () {
				NProgress.done();
			}); 
		}
	}
});