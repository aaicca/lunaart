const SearchOrderComponent = Vue.component("search-order",{
	props:{
		step: 0,
	},
	template: 
	`<b-input-group class="mt-3">
	
	    <b-form-input v-model="search" @input="searchOrder" placeholder="SEARCH ORDER" autocomplete="off" />
	  </b-input-group>`,
	data() {
		return {
			search: null,
			waiting: null,
			filter: null,
			dText: "All",
		}
	},
	created(){
	},
	update(){

	},
	methods: {
		customFormatter(date) {
		  return moment(date).format('MM - DD - YYYY');
		},
		searchOrder(e){
			clearTimeout(this.waiting);
			var main = this;

			this.waiting = setTimeout(function () {
				console.log('Input Value:', main.search);
				main.$root.$emit("searcho",{par: main.search, arg: main.filter});
			}, 100);
		},
		filterS(str){
			switch(str){
				case 'a':

					this.dText = "All";
				break;

				case 'c':
					this.dText = "Client";
				break;

				case 'e':
					this.dText = "Date";
				break;

				default:
					this.dText = "All";
				break;
			}

			this.filter = str;
		}
	}
});