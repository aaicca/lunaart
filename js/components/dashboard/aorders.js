const AOrderComponent = Vue.component("aorder",{
	template: 
	`<div class="cs-container">

		<b-container fluid v-if="!orderInfo">
			<b-row>
<<<<<<< HEAD
				<b-col sm=12 md=6 >
=======
				<b-col sm=12 md= >
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
					<h4 class="component-title padding">{{ $root.$data.componentTitle }}</h4>
				</b-col>
				<b-col sm=12 md=6>
					<search-order></search-order>
				</b-col>
			</b-row>

            <b-row class="text-center padding" v-if=loading>
              <b-col>
                <l-spinner color="luna-text-gold"></l-spinner>
              </b-col>
            </b-row>
			
			<b-row>
				<b-col class="padding">

	                <div class="table-responsive">
	                    <table class="table table-hover text-center" style="width: 100%!important">
	                      <thead>
	                        <tr>
	                          <th v-for="f in infields" scope="col">{{ f }}</th>
	                        </tr>
	                      </thead>
	                      <tbody>
	                        <tr v-for="ord in orders" @click="openOrder($event,ord.number)">
	                          <th scope="row">#{{ ord.number }}</th>
	                       	  <td>{{ ord.fdated }}</td>
	                       	  <td>{{ ord.name }} {{ ord.lastname }} {{ (ord.business != "" ? '('+ord.business+')' : "" ) }}</td>
	                          <td class="text-center"><button @click.prevent="viewOrder('view?o='+ord.number)" target="blank_"> <i class="fas fa-file-pdf"></i></button> </td>
	                          
	                          <td>{{ ord.process }}%</td>
	                          <td>{{ (ord.comment != "" ? ord.comment : "No comments") }}</td>
	                        </tr>
	                      </tbody>
	                    </table>
	                </div>

                </b-col>
            </b-row>
		</b-container>

		<b-container v-else>
			<b-row class="text-center padding" v-if='viewLoading'>
              <b-col>
                <l-spinner color="luna-text-gold"></l-spinner>
              </b-col>
            </b-row>

			<div v-else>
				
				<div v-if="order">
				
					<b-row>
						<b-col sm=2>
		                  <button class="btn-flat padding" style="padding: 2%;" @click="closeO"><i class="fas fa-chevron-left fa-2x"></i></button>
		                </b-col>

		                <b-col>	
		                	 <button class="btn-flat padding" @click.prevent="viewOrder('view?o='+order.number)" target="blank_"> <i class="fas fa-file-pdf fa-2x"></i></button>
		                </b-col>
					</b-row>
					<b-row>
						<b-col>
							<h5 class='gold-flat-text'> {{ order.title }} </h5>
							<b-card>
								<span class='grey-text'>{{ order.fdated }}</span> <br>
								<span class='font-boldy'>Number: #{{ order.number }}</span> <br>
								<span class='font-boldy'>Client / Company:</span> {{ order.name+' '+order.lastname+' '+(order.business != '' ? '('+order.business+')' : '' ) }} <br>
								<span class="font-boldy"> Pattern name: </span> {{ order.content.pattern.title }} <br>
							</b-card>
						</b-col>
					</b-row>

					<b-row>
						<b-col>
							<h5 class='gold-flat-text'>Details: </h5>
							<b-card>
								<span class="font-boldy"> Quantity: </span> {{ order.content.details.quantity }} <br>
								<span class="font-boldy"> SF required to cover: </span> {{ order.content.details.sfCovered }} <br>
								<span class="font-boldy"> SF total that order covers: </span> {{ (order.content.details.sf*order.content.details.quantity).toFixed(4) }} <br>
								<span class="font-boldy"> PO: </span> $ {{ order.content.details.po }} <br> <br>

								<span class="font-boldy"> Client extra details: </span> {{ order.clientComment }} <br>
							</b-card>

							<b-card>
								<span class="font-boldy"> Status: </span> {{ statusText[order.approval] }} <br>
								<span v-if="order.adated != null"> {{ order.adated }} </span> 
							</b-card>

						</b-col>
					</b-row>

					<b-row>
						<b-col sm=4 md=3 v-for='m in order.content.build'>
							<b-card :img-src="m.img" img-alt="Image" img-top>
								<p>
									<span class='font-boldy'>{{ m.title }}</span> <br>
									<span v-if="m.finish != null">Finish: {{ m.finish.text }}</span>
								</p>
								
							</b-card>
						</b-col>
					</b-row>

					<b-row>
						<b-col>
							<h5 class='gold-flat-text'>Received by: </h5>
							<b-card>
								<span>{{ order.admin.name+' '+order.admin.lastname}}</span> <br>
								<span>{{ order.admin.email }}</span>
							</b-card>

						</b-col>
					</b-row>

					<b-row v-if="errors.length">
						<b-col sm=12 v-for="error in errors">
							<b-alert variant="danger" show>{{ error.message }}</b-alert>
						</b-col>
					</b-row>

					<b-row>

						<b-col sm=12>
							<h5 class='gold-flat-text'> Quality control </h5>
							<b-card>
								<b-form-group label="Order progress">
									<b-input-group size="lg" append="%">
										<b-form-input v-model="updateForm.process" type="number" min=0 max=100 autocomplete="off" ></b-form-input>
									</b-input-group>
								</b-form-group>
								
								<b-form-group label="Set order priority">
									<b-form-select v-model="updateForm.priority" :options="options"></b-form-select>
								</b-form-group>

								<b-form-group label="Set invoice to this order: ">
                                  <b-form-file
                                    v-model="updateForm.invoice"
                                    placeholder="Chose invoice file"
                                    drop-placeholder="Drop file here..."
                                    accept="application/pdf"
                                  />
                                </b-form-group>

								<b-form-textarea
									v-model="updateForm.comment"
									placeholder="Order comments..."
									rows="2"
									max-rows="2"
								></b-form-textarea>
							</b-card>
						
						</b-col>
					</b-row>

					<b-row>
						<b-col>
							<span class='font-boldy'>Update production images</span>
							<vue-dropzone ref="myVueDropzone" id="productionImages" :options="dropzoneOptions"></vue-dropzone>
						</b-col>
					</b-row>
						
					<b-row>
						<b-col class="text-right padding">
							<button type="button"  @click='updateOrder' :disabled="uploading" class="btn-gold">UPDATE ORDER</button>
						</b-col>
					</b-row>

				</div>
				
				<b-row v-else>
					<b-col>
						<b-card>
							<span>
								Order not found
							</span>
						</b-card>
					</b-col>
				</b-row>

			</div>

		</b-container>
	</div>
	  `,
	data() {
		return {
			orderInfo: false,
			order: [{content: {details: {},}}],

			viewLoading: false,

			orders: [],

			errors: [],
			warn: [],

			statusText: ["Pending", "Approved", "Disapproved"],

			loading: false,
			infields: ["ORDER","DATE", "CLIENT","PDF","PROCESS","DESCRIPTION"],

			updateForm: {
				process: 0,
				comment: "",
				order: {},
				priority: null,
				invoice: null,
			},

			uploading: false,

			dropzoneOptions: {
		          url: 'orders',
		          thumbnailWidth: 150,
		          autoProcessQueue: false,
		          addRemoveLinks: true
		    },

			
			options: [
				{ value: null, text: 'Select priority' },
				{ value: 1, text: 'High' },
				{ value: 2, text: 'Normal' },
				{ value: 3, text: 'Not priority' },
			],

			
		    vd: new VueDiana()
		}
	},
	created(){
		let main = this;
		this.$root.$data.componentTitle = "Admin orders";
		this.$root.$on("searcho",function(e){
			main.searchO(e.par,e.arg);
		});
		this.searchO("a");
	},
	update(){
	},
	methods: {
		debug(){
			console.log(this.$refs.myVueDropzone.getQueuedFiles())
		},
		viewOrder(url){
			window.open(url);
		},
		updateOrder(){
			

			var main = this;	

			main.errors = [];

			if(main.updateForm.process > 100){
				main.errors.push({message: "Process can't be higher than 100%"});
				return false;
			}

			NProgress.start();

			var files = this.$refs.myVueDropzone.getQueuedFiles();
			var uploadFiles = [];
			console.log(this.$refs.myVueDropzone.getQueuedFiles())


			var submitForm = new FormData();

			main.updateForm["order"] = main.order;

			console.log(main.updateForm);

			for(var img in files){
				var ImageURL = files[img].dataURL;

				var block = ImageURL.split(";");

				var contentType = block[0].split(":")[1];

				var realData = block[1].split(",")[1];

				submitForm.append("processGallery[]", main.vd.b64toBlob(realData, contentType));
			}
			
			
			for(var input in main.updateForm){
				if(input == "order"){

					submitForm.append(input,JSON.stringify(main.updateForm[input]));
				}else{
					 submitForm.append(input,main.updateForm[input]);
				}
			}

			submitForm.append("invoice",main.updateForm.invoice);

		    submitForm.append("request","update");
		    submitForm.append("method","post");
		   

			axios.post('orders',submitForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		    }).then(function (response) {
	            //handle success
	            console.log(response);
	            console.log(response.data);

	            let call = response.data.call;

	            main.errors = [];

	             switch(call){

	               case true:
	                 main.vd.toastSuccess();
	               break;

	               case false:
	                 switch(response.data.errors){

	                   case "EmailExist":
	                     main.vd.toastError();
	                   break; 
	                 }
	               break;
	               
	               default:
	                 main.vd.toastError();
	               break;
	             }
		    }).catch(function (response) {
	            //handle error
	            console.log(response);
		    }).then(function () {
		        NProgress.done();
			});
		},
		searchO(par,args=""){
		      var vd = new VueDiana();
		      var main = this;
		      main.loading = true;

		      main.orders = [];

		      axios({
		        method: 'get',
		        url: 'orders?g='+par+'&args='+args,
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		             	main.orders = response.data.info;

		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
		                   vd.toastError();
		                 break;
		               }
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		        main.loading = false;
		      });  
		},
		openOrder(e,order){
			NProgress.start();

			let main = this;

			main.viewLoading = true;

			console.log(main.order)

			main.order = [{content: {details: {},}}];

			this.loadOneOrder(order);
		},
		loadOneOrder(number){
			console.log(number)
			let main = this;
			
			main.order = [{content: {details: {},}}];


			axios.get('orders?g=o&n='+number).then(function(response){
			  console.log(response)
			  if(response.data.hasOwnProperty("info")){
			    main.order = response.data.info[0];

			    main.updateForm.process =  parseInt(response.data.info[0].process);
		        
			  }else{
			  	main.order = [];
			  	main.updateForm.order = [];
			  	main.updateForm.process = 0;
			  }
			  
			}).catch(function(error){
			  console.log(error);
			}).then(function () {
		        main.viewLoading = false;
				
	        });

			
			this.orderInfo = true;
			NProgress.done();
			
		},
		closeO(){
			this.orderInfo = false;
		}
	}
});
