const ARequestComponent = Vue.component("arequest",{
	template: 
		`<div>
			<b-container fluid v-if="!requestInfo">
				<b-row>
					<b-col sm=12 md=6>
						<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
					</b-col>
				</b-row>

				<b-row>
					<b-col>
						<div class="table-responsive table-hover">
							<table class="table text-center with-grid" style="width: 100%!important">
							<thead>
								<tr>
									<th v-for="f in infieldsRequests" scope="col">{{ f }}</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="req in requests" @click="openRequest(req.id)">
									
									<td>{{ req.fdated }}</td>
									
									<td>
										<b-img fluid :src="req.img" style="max-width: 64px;"></b-img> <br/>
										<span>{{ req.content.pattern.title }}</span>
									</td>

									<td>{{ req.name+' '+req.lastname }} {{ ( req.business != "" ? '('+req.business+')' : '' ) }}</td>

									<td v-if="req.pdf">
										<b-button block v-on:click.stop.prevent="viewOrder(req.id)"><i class="fas fa-file-pdf fa-1x"></i></b-button> 
									</td>
									
									<td v-else><span class="grey-text">Pending</span></td>

									<td v-if="req.assignedTo != null">{{ req.assignedTo }}</td>
									<td v-else><span class="grey-text">Pending</span></td>
								</tr>
							</tbody>
							</table>
						</div>
					</b-col>
				</b-row>
			</b-container>

			<b-container v-else>
				<b-row style="padding-top:1%; padding-bottom:1%;" class="text-center">
					<b-col>
						<button class="btn-flat padding" style="padding: 1%;" @click="requestInfo = false"><i class="fas fa-chevron-left fa-2x"></i></button>
					</b-col>
					<b-col>
						<button class="btn btn-blue padding" style="padding: 2%;" @click="viewRequest"><i class="fas fa-file-pdf fa-1x"></i></button>
					</b-col>
					<b-col>
						<button class="btn btn-blue padding" style="padding: 2%;" @click="saveRequest">SAVE</button>
					</b-col>
					<b-col>
						<button class="btn btn-flat padding" @click="deleteRequest">
							<i class="material-icons">
								delete
							</i>
						</button>
					</b-col>
				</b-row>
				

				<b-row>
					<b-col xs=12 sm="3">
						<b-img fluid :src="requestCheck.img"></b-img> <br>
						<span>{{ requestCheck.content.pattern.title }}</span>
					</b-col>

					<b-col xs=12 sm="9">
						<b-row>
							<b-col cols=12><span class="font-boldy">User:</span> {{ requestCheck.name+' '+requestCheck.lastname }} {{ ( requestCheck.business != "" ? '('+requestCheck.business+')' : '' ) }}</b-col>
							<b-col cols=12>{{ requestCheck.fdated }}</b-col>
							<b-col cols=12>
								<b-form-group
									label="Assigned to:"
									description="Set which company was assigned this request"
								>
									<b-form-input
										v-model="assignedTo"
										type="text"
										placeholder="Ex. LuballSoftware company"
										autocomplete=off
									></b-form-input>
								</b-form-group>
							</b-col>
						</b-row>
					</b-col>
				</b-row>
			</b-container>
		</div>`,
	data() {
		return {
			infieldsRequests: ["DATE", "USER", "ORDER DETAILS", "PDF", "ASSIGNED TO"],

			requests: [],
			
			vd: new VueDiana(),

			requestInfo: false,

			requestCheck: {},

			assignedTo: null,
		}
	},
	created(){
		this.$root.$data.componentTitle = "Users requests";
		this.getRequests();
		console.log("My request")
	},
	update(){

	},
	methods: {
		viewRequest(){
			this.vd.toastSuccess("","What do you want here?");
		},
		openRequest(id){
			var main = this;
			NProgress.start();

			main.requestCheck = [];
			main.assignedTo = null;

			axios({
				method: 'get',
				url: 'my-request?g=one&id='+id,
			}).then(function (response) {
				//handle success
				console.log(response);
				console.log(response.data);

				let call = response.data.call;

				switch(call){

					case true:

						main.requestCheck = response.data.info[0];

						main.assignedTo = (main.requestCheck.assignedTo != null ? main.requestCheck.assignedTo : null);
						main.requestInfo = true;

					break;

					case false:
						switch(response.data.errors){

							default:
								main.vd.toastError();
							break;
						}
					break;
					
					default:
						main.vd.toastError();
					break;
				}
			}).catch(function (response) {
				//handle error
				console.log(response);
			}).then(function () {
				NProgress.done();
				main.$forceUpdate();
			});  
		},
		getRequests(){
			var main = this;
			NProgress.start();

			main.requests = [];

			axios({
				method: 'get',
				url: 'my-request?g=all',
			}).then(function (response) {
				//handle success
				console.log(response);
				console.log(response.data);

				let call = response.data.call;

				switch(call){
					case true:
						main.requests = response.data.info;
					break;

					case false:
						switch(response.data.errors){

							default:
								main.vd.toastError();
							break;
						}
					break;

					default:
						main.vd.toastError();
					break;
				}
			}).catch(function (response) {
				//handle error
				console.log(response);
			}).then(function () {
				NProgress.done();
				main.$forceUpdate();
			});  
		},
		saveRequest(){
			var main = this;
			var updateForm = new FormData();
				NProgress.start();	

			updateForm.append("id",main.requestCheck.id);
			updateForm.append("assignedTo",main.assignedTo);

			updateForm.append("request","update");
			updateForm.append("method","post");

			axios.post('my-request',updateForm,{
				headers: {
					'content-type': 'multipart/form-data'
				}
			}).then(function (response) {
					//handle success
					console.log(response);
					console.log(response.data);

					main.vd.toastSuccess();
					main.getRequests();
					main.requestInfo = false;
			}).catch(function (response) {
				console.log(response);
			}).then(function () {
				NProgress.done();
			});
		},
		deleteRequest(){
			var main = this;
			var updateForm = new FormData();
				

			if(confirm("Are you sure that you want to delete this request?")){
				NProgress.start();
				updateForm.append("id",main.requestCheck.id);

				updateForm.append("request","delete");
				updateForm.append("method","post");

				axios.post('my-request',updateForm,{
					headers: {
						'content-type': 'multipart/form-data'
					}
				}).then(function (response) {
						console.log(response);
						console.log(response.data);

						let call = response.data.call;

						main.vd.toastSuccess();
						main.getRequests();
						main.requestInfo = false;	

				}).catch(function (response) {
						//handle error
						console.log(response);
				}).then(function () {
					NProgress.done();
				});
			}			
		}
	}
});