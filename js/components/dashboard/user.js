const UserComponent = Vue.component("main-user",{
  template: `
            <div class="cs-container">
           
                <b-container fluid v-if=moreInfo>
                    <b-container class="padding">
                      <b-row>
                        <b-col>
                          <button class="btn-flat padding" style="padding: 2%;" @click="closeU"><i class="fas fa-chevron-left fa-2x"></i></button>
                        </b-col>
                        <b-col>
                          <button class="btn-blue valign" style="padding: 2%;" @click="saveU">SAVE</button>
                        </b-col>
                        <b-col  class="text-right">
                          <button class="btn-flat" style="padding: 2%;" @click="deleteU"><i class="fas fa-trash fa-2x"></i></button>
                        </b-col>
                      </b-row>
                    </b-container>

                    <b-tabs class="nav-justified" content-class="mt-3">
                      <b-tab title="Details" active>
                        <b-form>
                         <b-form-row>
                           <b-col class="padding text-center">
                              <div class="image-upload">

                                <label for="file-input">
                                  <b-img fluid style="width: 164px;" :src="infoU.img" for=iu />
                                </label>
                                <span>Upload</span>
                                <input id="file-input" v-on:change="setImage" class="hidden" type="file" accept="image/*" />
                              </div>
                           </b-col>
                         </b-form-row>
                           <b-form-row>
                             <b-col sm=12 md=6>
                               <b-form-group
                                label="Name"
                                >
                                  <b-form-input
                              
                                  type="text"
                                  placeholder="NAME"
                                  v-model="infoU.name"
                                  required/>
                                </b-form-group>
                              </b-col>

                              <b-col sm=12 md=6>
                               <b-form-group
                                label="Lastname"
                                >
                                  <b-form-input
                              
                                  type="text"
                                  placeholder="LASTNAME"
                                  v-model="infoU.lastname"
                                  required/>
                                </b-form-group>
                              </b-col>
                            </b-form-row>

                            <b-form-row>
                             <b-col sm=12>
                               <b-form-group
                                label="Company"
                                >
                                  <b-form-input
                              
                                  type="text"
                                  placeholder="COMPANY"
                                  v-model="infoU.business"
                                  required/>
                                </b-form-group>
                              </b-col>

                              <b-col sm=12>
                               <b-form-group
                                label="Email"
                                >
                                  <b-form-input
                              
                                  type="text"
                                  placeholder="EMAIL"
                                  v-model="infoU.email"
                                  required/>
                                </b-form-group>
                              </b-col>

                              <b-col sm=12>
                               <b-form-group
                                label="Phone"
                                >
                                  <b-form-input
                              
                                  type="text"
                                  placeholder="PHONE"
                                  v-model="infoU.phone"
                                  required/>
                                </b-form-group>
                              </b-col>
                              

                              <b-col sm=12>
                               <b-form-group
                                label="Reward percentage"
                                >
                                  <b-form-input
                              
                                  type="number"
                                  step=any
                                  placeholder="REWARD PERCENTAGE"
                                  v-model="infoU.reward"
                                  required/>
                                </b-form-group>
                              </b-col>


                              <b-col sm=12>
                               <b-form-group
                                label="Change password"
                                >
                                  <b-form-input
                              
                                  type="text"
                                  placeholder="PASSWORD"
                                  v-model="newpass"
                                  required/>
                                </b-form-group>
                              </b-col>
                            </b-form-row>
                         </b-form>

                         <b-row>
                          <b-col>
                            Level: {{ (infoU.role > 1 ? "Client" : "Admin") }} <br>
                            Address: {{ infoU.address }} <br>
                            City: {{ infoU.city }} <br>
                            State: {{ infoU.state }} <br>
                            ZP: {{ infoU.zp }}
                          </b-col>
                         </b-row>

                         <b-row>
                         <b-col>
                            <p class=text-grey>User registered: <br> {{ infoU.dated }}</p>
                         </b-col>
                       </b-row>
                      </b-tab>
                      <b-tab title="Invoices" v-if="infoU.role > 1"  @click=loadInv>
                        <b-row v-if="invErrors.length">
                          <b-col sm=12 v-for="error in invErrors">
                            <b-alert variant="danger" show>{{ error.message }}</b-alert>
                          </b-col>
                        </b-row>
                        <b-row>
                          <b-col sm=12 class="padding">
                            <b-collapse id="collapse2">
                              <b-card>
                                <b-form-group>
                                   <div class="mt-3">Selected file: {{ invsU.file ? invsU.file.name : '' }}</div>
                                  <b-form-file
                                    v-model="invsU.file"
                                    placeholder="Chose invoice file"
                                    drop-placeholder="Drop file here..."
                                    accept="application/pdf"
                                  />
                                </b-form-group>
                                <b-form-group>
                                  <b-form-input v-model="invsU.number" placeholder="ORDER" />
                                </b-form-group>
                                <b-form-group>
                                  <b-form-textarea
                                    v-model="invsU.desc"
                                    placeholder="Invoice description..."
                                    rows="3"
                                    max-rows="6"
                                  />
                                </b-form-group>

                                <b-col class="text-right">
                                  <button @click="uploadInv" class="btn-blue">Upload</button>
                                </b-col>
                              </b-card>
                            </b-collapse>
                            
                          </b-col>
                        </b-row>
                        <b-row>
                          <b-col v-if=loading>
                            <b-col class="text-center padding">
                              <l-spinner color="luna-text-gold"></l-spinner>
                            </b-col>
                          </b-col>

                          <b-col v-if="invsU.length && !loading">
                              <div class="table-responsive">
                                <table class="table" style="width: 100%!important">
                                  <thead>
                                    <tr>
                                      <th v-for="f in infields" scope="col">{{ f }}</th>
                                      <th><b-button v-b-toggle.collapse2 class="m-1">Add Invoice</b-button></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr v-for="inv in invsU">
                                      <th scope="row">#{{ inv.number }}</th>
                                      <td class="text-center"><a :href="inv.pdf" target="blank_"><i class="fas fa-file-pdf"></i></a> </td>
                                      <td>{{ inv.description }}</td>
                                      <td>{{ inv.dated }}</td>
                                      <td class="text-center"><button type="button" @click="deleteInv(inv.id)" class="btn-flat"><i class="fas fa-times"></i></button></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                              
                          </b-col>

                          <b-col class="text-center" v-if="!invsU.length && !loading">
                            <p>There's no invoices of this user, add the first <b-button v-b-toggle.collapse2 variant="link">here</b-button> </p>
                          </b-col>
                        </b-row>
                      </b-tab>

                      <b-tab title="Orders" v-if="infoU.role > 1" @click="loadOrder">
                        <b-row class="text-center padding" v-if=loading>
                          <b-col>
                            <l-spinner color="luna-text-gold"></l-spinner>
                          </b-col>
                        </b-row>
                        
                        <b-row>
                            <b-col class="padding">

                              <div class="table-responsive">
                                  <table class="table text-center" style="width: 100%!important">
                                    <thead>
                                      <tr>
                                        <th v-for="f in infields" scope="col">{{ f }}</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr v-for="ord in orders">
                                        <th scope="row">#{{ ord.number }}</th>
                                         <td>{{ ord.fdated }}</td>
                                         
                                        <td class="text-center"><button @click.prevent="viewOrder('view?o='+ord.number)" target="blank_"> <i class="fas fa-file-pdf"></i></button> </td>
                                        
                                        <td>{{ ord.process }}%</td>
                                        <td>{{ (ord.comment != "" ? ord.comment : "No comments") }}</td>
                                      </tr>
                                    </tbody>
                                  </table>
                              </div>

                            </b-col>
                        </b-row>

                      </b-tab>

                      <b-tab title="Packing list" v-if="infoU.role > 1" @click="loadPacking">
                          <b-container fluid>
                            
                            
                            <b-row v-if="invErrors.length" class='padding'>
                                    <b-col sm=12 v-for="error in packErrors">
                                      <b-alert variant="danger" show>{{ error.message }}</b-alert>
                                    </b-col>
                                  </b-row>

                            <b-row>
                                    <b-col sm=12 class="padding">
                                      <b-collapse id="collapse-2">
                                        <b-card>
                                        
                                        <b-form-group>
                                          <b-form-input v-model="packU.number" placeholder="ORDER" list='orders' autocomplete="off" @input="searchOrders(packU.number)" />

                                          <datalist id="orders">
                                            <option v-for='o in sortOrders' :value="o.number">{{ o.name+' '+o.lastname+' '+(o.business != ''? '('+o.business+')' : '')}}</option>
                                          </datalist>
                                        </b-form-group>

                                      <b-form-group>
                                        <b-form-input v-model="packU.track" type="number" placeholder="TRACKING NUMBER" autocomplete="off" />
                                      </b-form-group>

                                      <b-form-group>
                                        <b-form-input v-model="packU.carrier" type="text" placeholder="CARRIER" autocomplete="off" />
                                      </b-form-group>
                                
                                <b-form-group>
                                        <b-input-group append='Lbs'>
                                    <b-form-input type="number" step=any v-model="packU.weight" placeholder="WEIGHT" autocomplete="off" />
                                  </b-input-group>
                                      </b-form-group>

                                      <b-form-group label="STATUS">
                                  <b-form-select v-model="packU.status" :options="statusOpt" :value="1" ></b-form-select>
                                </b-form-group>

                                <b-form-group>
                                        <b-form-input type="number" v-model="packU.boxes" placeholder="BOXES" autocomplete="off" />
                                      </b-form-group>
                                
                                <b-row>
                                        <b-col sm=6>
                                    <b-form-group label="ORIGIN">
                                      <b-form-select v-model="packU.origin" :options="states" ></b-form-select>
                                    </b-form-group>
                                  </b-col>

                                  <b-col sm=6>
                                    <b-form-group label="DESTINY">
                                      <b-form-select v-model="packU.destiny" :options="states" ></b-form-select>
                                    </b-form-group>
                                  </b-col>
                                      </b-row>

                                      <b-row>
                                        <b-col sm=6>
                                          <b-input-group append='"'>
                                      <b-form-input type="number" step=any v-model="packU.w" placeholder="WIDTH" autocomplete="off" />
                                    </b-input-group>
                                  </b-col>

                                  <b-col sm=6>
                                    <b-input-group append='"'>
                                      <b-form-input type="number" step=any v-model="packU.h" placeholder="HEIGHT" autocomplete="off" />
                                    </b-input-group>
                                  </b-col>
                                      </b-row>

                                          <b-col class="text-right padding">
                                            <button @click="uploadPack" class="btn-blue">Upload</button>
                                          </b-col>
                                        </b-card>
                                      </b-collapse>
                                      
                                    </b-col>
                                  </b-row>
                            
                            <b-row class="text-center padding" v-if='loading'>
                              <b-col>
                                <l-spinner color="luna-text-gold"></l-spinner>
                              </b-col>
                            </b-row>
                            
                            <b-row v-else>
                              <b-col sm=12 v-if="packings.length">
                                <b-col class="padding">

                                          <div class="table-responsive">
                                              <table class="table table-hover text-center" style="width: 100%!important">
                                                <thead>
                                                  <tr>
                                                    <th v-for="f in infieldsTrack" scope="col">{{ f }}</th>
                                                    <th><b-button v-b-toggle.collapse-2 class="m-1">Add Packing list</b-button></th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  <tr v-for="pac in packings">
                                                    <th scope="row">#{{ pac.number }}</th>
                                                     <td>{{ pac.track }}</td>
                                                     <td>{{ pac.fdated }}</td>
                                                     <td>{{ pac.boxes }}</td>
                                                     <td>{{ pac.weight }}Lbs</td>
                                                     <td>{{ pac.width }}" x {{ pac.height }}"</td>
                                                     <td>{{ statusDisplay[pac.status] }}</td>
                                                     <td>{{ pac.carrier }}</td>
                                                     <td>{{ pac.origin }}</td>
                                                     <td>{{ pac.destiny }}</td>
                                                     <td class="text-center full-width" style='display: inline-block'><button type="button" @click="deletePack(pac.id)" class="btn-flat"><i class="fas fa-times"></i></button></td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                          </div>

                                        </b-col>
                              </b-col>
                            
                              <b-col sm=12 class="text-center" v-else>
                                <p class="text-center" >There's no packing lists, add one here <b-button v-b-toggle.collapse-2 variant="link">Link</b-button></p>
                              </b-col>
                            </b-row>


                          </b-container>
                      </b-tab>
                    </b-tabs>

                </b-container>

                <b-container fluid v-else>
                    <b-row>
                      <b-col sm=12 >
                        <h4 class="component-title padding">{{ $root.$data.componentTitle }}</h4>
                      </b-col>
                    </b-row>
                    
                    <b-tabs class="nav-justified">
                      <b-tab title="Search" active>
                        <search-user></search-user>
                        
                        <b-row class="text-center padding" v-if=loading>
                          <b-col>
                            <l-spinner color="luna-text-gold"></l-spinner>
                          </b-col>
                        </b-row>

                        <b-list-group v-else style="padding-top:1%">
                          <b-list-group-item href="#" v-for="u in users" @click="openU(u)" class="flex-column align-items-start" :class="(u.role < 2 ? 'luna-gold luna-goldHover text-white' : '')">
                            <div class="d-flex w-100 justify-content-between">
                              <h6 class="mb-1">{{ u.name }} {{ u.lastname }}</h6>
                              <small class="text-muted">{{ u.fdated }}</small>
                            </div>

                            <p class="mb-1">
                              {{ u.email }} <br>
                              {{ u.phone }} <br>

                            </p>

                            <small class="text-muted">{{ u.business }}</small>
                          </b-list-group-item>
                        </b-list-group>
                        
                        
                      </b-tab>
                      <b-tab title="New user">
                        <new-user></new-user>
                      </b-tab>
                    </b-tabs>
<<<<<<< HEAD
                </b-container>
=======
              </b-container>
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
            </div>`,
  data(){
    return {
      notifications:[],
      users: [],
      empty: [],
      loading: false,
      moreInfo: false,
      nfile: null,
      newpass: null,
      infields: ['ORDER', 'FILE', 'DESCRIPTION', "DATE"],
      invErrors: [],
      invsU: {
      },
      infoU: [],

      infields: ["ORDER","DATE","PDF","PROCESS","DESCRIPTION"],
      orders: [],

      statusOpt: [
        {value: 1, text: "Preparing"},
        {value: 2, text: "Shipped"},
        {value: 3, text: "In transit"},
        {value: 4, text: "Delivered"},
        {value: 5, text: "Canceled"},
      ],

      packU: {},
      packings: [],

      sortOrders: [],

      packErrors: [],
      states: [],

      statusDisplay: false,

      infieldsTrack: ["ORDER", "TRACKING NUMBER", "DATE", "TOTAL BOXES", "TOTAL WEIGHT", "SIZE", "STATUS", "CARRIER", "ORIGIN", "DESTINY"],

      vd: new VueDiana()
    }
  },
  created(){
     let main = this;
     this.$root.$data.componentTitle  = "Admin users";
     this.$root.$on("searchu",function(e){
        main.searchU(e.par,e.arg);
     });
     this.searchU("new")
     console.log(this.$root);


      axios.get('https://gist.githubusercontent.com/mshafrir/2646763/raw/8303e1d831e89cb8af24a11f2fa77353c317e408/states_titlecase.json').then(function(response){
        let lalas = response.data;
        console.log(lalas)
        console.log(main.states);
        for(var key in response.data){
          var temp = {
            text: response.data[key].name,
            value: response.data[key].abbreviation,
          }

          main.states.push(temp);
        }
        console.log(main.states);
      }).catch(function(error){
        console.log(error);
      });
  },
  methods:{
    viewOrder(url){
      window.open(url);
    },
    deletePack(id){
         var main = this;
         var updateForm =  new FormData();
         var vd = new VueDiana();

         if(confirm("Are you sure that you want delete packing list?")){
           updateForm.append("id",id);
           updateForm.append("request","delete");
           updateForm.append("method","post");

          axios.post('packing-lists',updateForm,{
            headers: {
               'content-type': 'multipart/form-data'
            }
          }).then(function (response) {
                //handle success
                console.log(response);
                console.log(response.data);

                let call = response.data.call;

                main.invErrors = [];

                 switch(call){

                   case true:
                     main.vd.toastSuccess();
                     main.loadPacking();
                   break;

                   case false:
                     switch(response.data.errors){

                       default:
                         main.vd.toastError();
                       break;
                     }
                   break;
                   
                   default:
                     main.vd.toastError();
                   break;
                 }
            }).catch(function (response) {
                //handle error
                console.log(response);
            }).then(function () {
            main.loading = false;
          });
         }
      },
    loadOrder(e){
      var main = this;

      axios({
        method: 'get',
        url: 'orders?g=uo&id='+main.infoU["id"],
      }).then(function (response) {
          //handle success
          console.log(response);
          console.log(response.data);

          let call = response.data.call;

          main.errors = [];

           switch(call){

             case true:
               if(response.data.hasOwnProperty("info")){
                  main.orders = response.data.info;
               }else{
                 main.orders = [];
               }
              
             break;

             case false:
               switch(response.data.errors){

                 default:
                   vd.toastError();
                 break;
               }
             break;
             
             default:
               vd.toastError();
             break;
           }

           main.loading = false;
      }).catch(function (response) {
          //handle error
          console.log(response);
      }).then(function () {
        main.loading = false;
      }); 
    },
    uploadPack(){

      var main = this;
      main.loading = true;

      var uploadForm = new FormData();

      for(var input in main.packU){
        uploadForm.append(input,main.packU[input]);
      }

      uploadForm.append("request","add");
      uploadForm.append("method","post");

       axios.post('packing-lists',uploadForm,{
            headers: {
               'content-type': 'multipart/form-data'
            }
          }).then(function (response) {
                //handle success
                console.log(response);
                console.log(response.data);

                let call = response.data.call;

                main.packErrors = [];

                 switch(call){

                   case true:
                     main.vd.toastSuccess();
                     main.searchPacking();
                   break;

                   case false:
                     switch(response.data.errors){
                        case "NumberAlready":
                          main.packErrors.push({message:"That order has already a packing lists"});
                        break;

                       default:
                         main.vd.toastError();
                       break;
                     }
                   break;
                   
                   default:
                     main.vd.toastError();
                   break;
                 }
            }).catch(function (response) {
                //handle error
                console.log(response);
            }).then(function () {
            main.loading = false;
          });

    },
    loadPacking(e){
      var main = this;

      axios({
        method: 'get',
        url: 'packing-lists?g=ou&id='+main.infoU["id"],
      }).then(function (response) {
          //handle success
          console.log(response);
          console.log(response.data);

          let call = response.data.call;

          main.errors = [];

           switch(call){

             case true:
               if(response.data.hasOwnProperty("info")){
                  main.packings = response.data.info;
               }else{
                 main.packings = [];
               }
              
             break;

             case false:
               switch(response.data.errors){

                 default:
                   vd.toastError();
                 break;
               }
             break;
             
             default:
               vd.toastError();
             break;
           }

           main.loading = false;
      }).catch(function (response) {
          //handle error
          console.log(response);
      }).then(function () {
        main.loading = false;
      }); 
    },
    setImage(e){
      try{
        this.nfile = e.target.files[0];

        this.infoU.img = URL.createObjectURL(this.nfile);
      }catch(ex){}
      
    },
    openU(u){
        this.moreInfo = true;
        this.infoU = u;
        this.newpass = null;
       console.log(u)
       console.log(this.infoU);
    },
    closeU(){
      this.moreInfo = false;
    },
    saveU(e){
      e.preventDefault();

      let main = this;
      main.loading = true;
      console.log("clic")

      var form = document.getElementById(this.formId);
      var updateForm = new FormData(form);

      var vd = new VueDiana();

      console.log(form);

      for(var input in this.infoU){
        updateForm.append(input,this.infoU[input]);
      }

      updateForm.append("img",this.nfile);
      updateForm.append("pass",this.newpass);

      updateForm.append("request","aupdate");
      updateForm.append("method","post");

      console.log(this.infoU);

      axios.post('user',updateForm,{
        headers: {
           'content-type': 'multipart/form-data'
        }
      }).then(function (response) {
            //handle success
            console.log(response);
            console.log(response.data);

            let call = response.data.call;

            main.errors = [];

             switch(call){

               case true:
                 main.vd.toastSuccess();
               break;

               case false:
                 switch(response.data.errors){

                   case "EmailExist":
                     main.errors.push({message:"That email is already in use"});
                   break; 

                   case "NotEnoughForm":
                     main.errors.push({message:"Must fill all fields in form"});
                   break;

                   case "ShortPass":
                     main.errors.push({message:"New password is too short, must be at least 6 characters"});
                   break; 

                   default:
                     main.vd.toastError();
                   break;
                 }
               break;
               
               default:
                 main.vd.toastError();
               break;
             }
        }).catch(function (response) {
            //handle error
            console.log(response);
        }).then(function () {
          main.loading = false;
        });
    },
    deleteU(e){
       var main = this;
       var updateForm =  new FormData();
       var vd = new VueDiana();

       if(confirm("Are you sure that you want delete this user?")){
         updateForm.append("id",this.infoU.id);
         updateForm.append("request","delete");
         updateForm.append("method","post");

        axios.post('user',updateForm,{
          headers: {
             'content-type': 'multipart/form-data'
          }
        }).then(function (response) {
              //handle success
              console.log(response);
              console.log(response.data);

              let call = response.data.call;

              main.errors = [];

               switch(call){

                 case true:
                   vd.toastSuccess();
                   main.loadInv();
                   main.closeU();
                   main.searchU("new");
                 break;

                 case false:
                   switch(response.data.errors){

                     default:
                       vd.toastError();
                     break;
                   }
                 break;
                 
                 default:
                   vd.toastError();
                 break;
               }
          }).catch(function (response) {
              //handle error
              console.log(response);
          }).then(function () {
          main.loading = false;
        });
       }
    },
    deleteInv(id){
       var main = this;
       var updateForm =  new FormData();
       var vd = new VueDiana();

       if(confirm("Are you sure that you want delete invoice?")){
         updateForm.append("id",id);
         updateForm.append("request","delete");
         updateForm.append("method","post");

         axios.post('invoice',updateForm,{
          headers: {
             'content-type': 'multipart/form-data'
          }
        }).then(function (response) {
              //handle success
              console.log(response);
              console.log(response.data);

              let call = response.data.call;

              main.invErrors = [];

               switch(call){

                 case true:
                   vd.toastSuccess();
                   main.loadInv();
                 break;

                 case false:
                   switch(response.data.errors){

                     default:
                       vd.toastError();
                     break;
                   }
                 break;
                 
                 default:
                   vd.toastError();
                 break;
               }
          }).catch(function (response) {
              //handle error
              console.log(response);
          }).then(function () {
<<<<<<< HEAD
            main.loading = false;
          });
=======
          main.loading = false;
        });
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
       }
    },
    uploadInv(e){
      e.preventDefault();

      let main = this;
      main.loading = true;
      console.log("clic")

      var form = document.getElementById(this.formId);
      var updateForm = new FormData(form);

      var vd = new VueDiana();

      console.log(form);

      for(var input in this.invsU){
        updateForm.append(input,this.invsU[input]);
      }

      updateForm.append("id",this.infoU["id"]);
      updateForm.append("request","add");
      updateForm.append("method","post");

      console.log(this.invsU);

      axios.post('invoice',updateForm,{
        headers: {
           'content-type': 'multipart/form-data'
        }
      }).then(function (response) {
            //handle success
            console.log(response);
            console.log(response.data);

            let call = response.data.call;

            main.invErrors = [];

             switch(call){

               case true:
                 vd.toastSuccess();
                 main.loadInv();
               break;

               case false:
                 switch(response.data.errors){

                   case "EmailExist":
                     main.invErrors.push({message:"That email is already in use"});
                   break; 

                   case "NotEnoughForm":
                     main.invErrors.push({message:"Is needed a file and a number"});
                   break;

                   case "NumberAlready":
                     main.invErrors.push({message:"That number already belongs to another invoice"});
                   break;

                   default:
                     vd.toastError();
                   break;
                 }
               break;
               
               default:
                 vd.toastError();
               break;
             }
        }).catch(function (response) {
            //handle error
            console.log(response);
        }).then(function () {
        main.loading = false;
      });
    },
    loadInv(e){
      var main = this;
      main.loading = true;
      main.invErrors = [];
      axios({
        method: 'get',
        url: 'invoice?r=a&i='+main.infoU["id"],
      }).then(function (response) {
          //handle success
          console.log(response);
          console.log(response.data);

          let call = response.data.call;

          main.errors = [];

           switch(call){

             case true:
               if(response.data.hasOwnProperty("info")){
                  main.invsU = response.data.info;
               }else{
                 main.invsU = [];
               }
              
             break;

             case false:
               switch(response.data.errors){

                 default:
                   vd.toastError();
                 break;
               }
             break;
             
             default:
               vd.toastError();
             break;
           }

           main.loading = false;
      }).catch(function (response) {
          //handle error
          console.log(response);
      }).then(function () {
        main.loading = false;
      }); 
    },
    searchU(par,args=""){
      var vd = new VueDiana();
      var main = this;
      main.loading = true;

      axios({
        method: 'get',
        url: 'user?g='+par+'&args='+args,
      }).then(function (response) {
          //handle success
          console.log(response);
          console.log(response.data);

          let call = response.data.call;

          main.errors = [];

           switch(call){

             case true:
               main.users = response.data.info;
             break;

             case false:
               switch(response.data.errors){

                 default:
                   vd.toastError();
                 break;
               }
             break;
             
             default:
               vd.toastError();
             break;
           }
      }).catch(function (response) {
          //handle error
          console.log(response);
      }).then(function () {
        main.loading = false;
      });  
    },
    searchOrders(number){
        var main = this;
        
        axios({
            method: 'get',
            url: 'orders?g=as&number='+number,
        }).then(function (response) {
              //handle success
              console.log(response);
              console.log(response.data);

              let call = response.data.call;

               switch(call){

                 case true:
                   main.sortOrders = response.data.info;

                 break;

                 case false:
                   switch(response.data.errors){

                     default:
                       main.vd.toastError();
                     break;
                   }
                 break;
                 
                 default:
                   main.vd.toastError();
                 break;
               }
        }).catch(function (response) {
              //handle error
              console.log(response);
        }).then(function () {
            main.loading = false;
        }); 
      },
  },
  
});