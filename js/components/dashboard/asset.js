const AssetComponent = Vue.component("asset",{
	template: 
	`<div class="cs-container">
		<b-container fluid>
			<b-row>
				<b-col sm=12 md=6>
					<h4 class="component-title">{{ $root.$data.componentTitle }}</h4>
				</b-col>
			</b-row>
			<b-tabs content-class="mt-3">
			    <b-tab title="Pattern">
			    	<b-row v-if="errorsColl.length">
                      <b-col sm=12 v-for="error in errorsColl">
                        <b-alert variant="danger" show>{{ error.message }}</b-alert>
                      </b-col>
                    </b-row>
					
					<b-row>
					  <b-col sm=12 md=3 class="padding text-right">
					  	<b-button block v-b-toggle.collapse1>Add pattern</b-button>
					  </b-col>
					  
					</b-row>

			    	<b-row>
						  <b-col sm=12>
							  <b-collapse id="collapse1">
								
								<b-tabs class="nav-justified">
									<b-tab title="Materialized pattern" active>
										<b-card>

									      <b-form-group>
		 								    <b-form-input v-model="collectionNew.title" type="text" placeholder="NAME" autocomplete="off" />
										  </b-form-group>

								    	  <b-form-group description="Image and materials that will be displayed in the landing page">
								    	  	  <b-col class="padding">
										  		<button @click="nm.push( { ms: [] } )" ><i class="fas fa-plus"></i></button>
										  		<button @click="nm.splice(-1,1)"><i class="fas fa-minus"></i></button>
										  	  </b-col>
			                                  <div v-for="(item,index) in nm" >

					                            <b-col sm=12>
					                            	<b-form-file
						                                v-model="item[index]"
						                                placeholder="Chose pattern display image..."
						                                drop-placeholder="Drop file here..."
						                                accept="image/*"
						                            />
											    	
													<b-form-group description="Add materials">
													  	<b-col class="padding">
													  		<button @click="nm[index]['ms'].push( { 'val': null } )" ><i class="fas fa-plus"></i></button>
													  		<button @click="nm[index]['ms'].splice(-1,1)"><i class="fas fa-minus"></i></button>
<<<<<<< HEAD
														</b-col> 
														
														<div v-for="(m,uindex) in nm[index]['ms']">
															<span>Section {{ parseInt(uindex)+1 }}</span>
															<b-form-select @input="addMtoC($event,index,uindex)" :options="materials" />
														</div>  
						                                
=======
													  	</b-col>
						                                <b-form-select v-for="(m,uindex) in nm[index]['ms']" @input="addMtoC($event,index,uindex)" :options="materials" />
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
						                            </b-form-group>
											    </b-col>
										  	  </div>
	
		                                  </b-form-group>

										  <b-form-group description="Select main pattern">
		                                    <b-form-select v-model="collectionNew.dp" :options="cdps" />
		                                  </b-form-group>
											
										  <b-form-group>
										  	<b-input-group prepend="$" >
													<b-form-input type="number" step="any" v-model="collectionNew.price" placeholder="PRICE" autocomplete="off" />
											</b-input-group>
										  </b-form-group>
		
		                                  <b-form-group>
		                                    <b-form-select v-model="collectionNew.category" :options="cCat" />
		                                  </b-form-group>

										  <b-col class="text-right padding">
		                                     <button @click="uploadCollection" class="btn-blue">Upload</button>
		                                  </b-col>
									    </b-card>
									</b-tab>
									<b-tab title="Clean pattern">

										<b-row v-if="errorsDp.length">
					                      <b-col sm=12 v-for="error in errorsDp">
					                        <b-alert variant="danger" show>{{ error.message }}</b-alert>
					                      </b-col>
					                    </b-row>
										  
										  <b-form-group>
		 								    <b-form-input v-model="patternNew.title" type="text" placeholder="NAME" autocomplete="off" />
										  </b-form-group>

		                                  <b-form-group description="Pattern image to be displayed clean">
			                                  <b-form-file
			                                    v-model="patternNew.ptImg"
			                                    placeholder="Chose pattern display image..."
			                                    drop-placeholder="Drop file here..."
			                                    accept="image/jpeg,image/png"
			                                  />
		                                  </b-form-group>

		                                  <b-form-group description="Pattern image clean and detailed with sizes">
			                                  <b-form-file
			                                    v-model="patternNew.ptDetail"
			                                    placeholder="Chose pattern detailed image..."
			                                    drop-placeholder="Drop file here..."
			                                    accept="image/jpeg,image/png"
			                                  />
		                                  </b-form-group>
								

		                                  <b-form-group description="Pattern image with chromatic colors that will be used in the design pattern app component">
			                                  <b-form-file
			                                    v-model="patternNew.chroma"
			                                    placeholder="Chose pattern chroma app image..."
			                                    drop-placeholder="Drop file here..."
			                                    accept="image/jpeg,image/png"
			                                  />


		                                  </b-form-group>

		                                  <b-form-group description="Colors that are in the Chroma pattern image to be replaced with materials">
		                                    <b-form-select v-model="patternNew.cchromas" :options="cc" />
		                                  </b-form-group>
										  
										  <b-form-group>
											  <b-input-group prepend="$" >
													<b-form-input type="number" step="any" v-model="patternNew.price" placeholder="PRICE" autocomplete="off" />
											  </b-input-group>

			                                  <b-input-group prepend="Width" append="&#34;" >
													<b-form-input type="number" v-model="patternNew.w" @input="sc" />
											  </b-input-group>

											  <b-input-group prepend="Height" append="&#34;" >
													<b-form-input type="number" v-model="patternNew.h" @input="sc" />
											  </b-input-group>

												<b-card>
													<strong>
														1 sheet covers
													</strong>
													<p>{{ patternNew.sc }} Square feet</p>
 												</b-card>
	
										  </b-form-group>

		                                  <b-form-group>
		                                    <b-form-select v-model="patternNew.category" :options="cCat" />
		                                  </b-form-group>

											<b-form-textarea
												v-model="patternNew.desc"
												placeholder="Description"
											/>

		                                  <b-col class="text-right padding">
		                                     <button @click="uploadPattern" class="btn-blue">Upload</button>
		                                  </b-col>
									</b-tab>
								  </b-tabs>	

							    
							  </b-collapse>
						  </b-col>
					</b-row>
					
					

					<b-row>
					  <b-col sm=12 class="padding text-right">
					  	<b-button @click="displayCDPB = !displayCDPB">View {{ (!displayCDPB ? "full collections" : "clean patterns") }} </b-button>
					  </b-col>
					  
					</b-row>

					<b-container fluid v-if="displayCDPB">
						<b-container fluid v-if="cloading">
							<b-col class="text-center padding">
								<l-spinner color="luna-text-gold"></l-spinner>
							</b-col>
						</b-container>
						
						<b-container fluid v-else>
							<collection :side=2 v-if="collections.length"></collection>

							<b-row v-else>
								<b-col class="text-center">
									<p>There's no collections in your library</p>
								</b-col>
							</b-row>
						</b-container>
						
					</b-container>

					<b-container fluid v-if="!displayCDPB">
						<b-container fluid v-if="cloading">
							<b-col class="text-center padding">
								<l-spinner color="luna-text-gold"></l-spinner>
							</b-col>
						</b-container>
						
						<b-container v-else>
							<a-displaydp :side=2 v-if="cdps.length"></a-displaydp>

							<b-row v-else>
								<b-col class="text-center">
									<p>There's no patterns in your library</p>
								</b-col>
							</b-row>
						</b-container>
						
					</b-container>
			    </b-tab>

			    <b-tab title="Materials">
	
					<b-row v-if="errorsMaterial.length">
                      <b-col sm=12 v-for="error in errorsMaterial">
                        <b-alert variant="danger" show>{{ error.message }}</b-alert>
                      </b-col>
                    </b-row>

					<b-row>
						  <b-col sm=12 class=padding>
						  	<b-button block v-b-toggle.collapse2>Add Material</b-button>
						  </b-col>

						  <b-col sm=12 >
							  <b-collapse id="collapse2" class="mt-2">
							    
							      <b-form-group>
	                                  <b-form-file
	                                    v-model="materialNew.img"
	                                    placeholder="Chose material image"
	                                    drop-placeholder="Drop file here..."
	                                    accept="image/*"
	                                  />
	                                </b-form-group>

	                                <b-form-group>
	                                  <b-form-input v-model="materialNew.title" placeholder="NAME" />
	                                </b-form-group>

	                                <b-form-group>
	                                  <b-form-select v-model="materialNew.category" :options="mCat" />
	                                </b-form-group>

	                                <b-col class="text-right">
	                                  <button @click="uploadMat" class="btn-blue">Upload</button>
	                                </b-col>
							    
							  </b-collapse>
						  </b-col>
					</b-row>

					<material :side=2></material>
			    </b-tab>
			</b-tabs>
		</b-container>
	 </div> `,
	data() {
		return {
			sizes: ["Hi","Ah","Lalas"],
			tag: '',
      		tags: [],

      		ms: [],
      		nm: [], //New images for 
      		displayCDPB: true,

			errorsColl: [],
			collections: [],
			collectionNew: {
				title: null,
				category: null,
				img: null,
				materials: [],
				desc: null,
				dp: null,
				price: null
			},

			errorsDp: [],
			cdps: [],
			patternNew: {
				title: null,
				category: null,
				ptImg: null,
				ptDetail: null,
				chroma: null,
				cchromas: null,
				w: 0,
				h: 0,
				sc: 0,
				desc: null,
			},

			collUpdate: [],
			cloading: false,
			cCat: [
				{text: 'Collection', value: null },
				{text: "Coastline",value: 1},
				{text: "Progresive",value: 2},
				{text: "Timeless",value: 3},
				{text: "Traditional",value: 4},
				{text: "Geometric",value: 5}
			],
			cc: [
				{text: 'CHROMAS QUANTITY', value: null },
				{text: "1 Green  (#3cff00)",value: 1},
				{text: "2 Blue   (#0024ff)",value: 2},
				{text: "3 Red    (#ff0000)",value: 3},
				{text: "4 Purple (#ff00fc)",value: 4},
				{text: "5 Cyan   (#00fff6)",value: 5}
			],

			/*
			{text: "1 Green  (#3cff00)",value: 1},
				{text: "2 Blue   (#0024ff)",value: 2},
				{text: "3 Red    (#ff0000)",value: 3},
				{text: "4 Yellow (#e4ff00)",value: 4},
				{text: "5 Cyan   (#00fff6)",value: 5}*/
			materialsAutocomplete: [],

			errorsMaterial: [],
			materials: [],
			materialNew: {
				title: null,
				category: null,
				img: null
			},
			mCat: [
				{text: 'Category', value: null },
				{text: "Stone",value: 1},
				{text: "Mirror",value: 2},
				{text: "Metal",value: 3},
				{text: "Glass",value: 4},
				{text: "Gemstones",value: 5}
			],
			materialUpdate: [],
			mReload: 0,
			mloading: false,

			dps: []

		}
	},
	mounted(){
		this.$root.$data.componentTitle = "Assets";
		this.loadPattern();
		this.loadMaterial();	
		this.loadCollection();
		console.log();
	},
	updated(){
	},	
	methods: {
		addMtoC(e,m,i){
<<<<<<< HEAD
			//Add material to collection
=======
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
			console.log(e);
			console.log(m);

			this.nm[m]["ms"][i] = e;
		},
		sc(){
			this.patternNew.sc = (parseFloat(this.patternNew.w)*parseFloat(this.patternNew.h))/144;
			this.patternNew.sc = this.patternNew.sc.toFixed(4);
		},
		uploadCollection(e){
			e.preventDefault();

		      let main = this;
		      main.cloading = true;

		      var updateForm = new FormData();
		      var vd = new VueDiana();

		      for(var input in this.collectionNew){
		        updateForm.append(input,this.collectionNew[input]);
		      }

		      for(var input in this.nm){
		      	if(input != "val"){
		      		updateForm.append("file-"+input,this.nm[input][input]);
		      		updateForm.append("materials[]",JSON.stringify(this.nm[input].ms));
		      	}

		      	console.log(this.nm[input][input]);
		      }

		      updateForm.append("request","collection");
		      updateForm.append("action","new");
		      updateForm.append("method","post");

		      console.log(updateForm.get("file-0"));

		      console.log(this.collectionNew);

		      axios.post('asset',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		      }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errorsColl = [];

		             switch(call){

		               case true:
		                 vd.toastSuccess();
		                 main.loadMaterial();
		                 window.dispatchEvent(new Event("reloadCollections"));
		                 main.$forceUpdate();

		               break;

		               case false:
		                 switch(response.data.errors){

		                   case "NotEnoughForm":
		                     main.errorsColl.push({message:"Must fill all fields in form"});
		                   break;

		                   case "NotFileMoved":
		                   	 main.errorsColl.push({message:"Couldn't move file to server"});
		                   break;

		                   case "CollectionAlready":
		                   	 main.errorsColl.push({message:"That collection already exist"});
		                   break;

		                   default:
		                     vd.toastError();
		                   break;
		                 }
		               break;
		               
		               default:
		                 vd.toastError();
		               break;
		             }
		        }).catch(function (response) {
		            //handle error
		            console.log(response);
		        }).then(function () {
		        main.cloading = false;
		      });

		  
		},
		loadCollection(e){

			var main = this;
			 main.collections = [];
			 axios({
		        method: 'get',
		        url: 'asset?r=collection&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  main.collections = response.data.info;
		               }else{
		                  main.collections = [];
		               }
		              
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		      	vm.$forceUpdate();
		      });
		},
		loadPattern(){
			var main = this;
			 main.cdps =[];
			 axios({
		        method: 'get',
		        url: 'asset?r=dp&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  main.cdps = response.data.info;
		               }else{
		                  main.cdps = [];
		               }
		              
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		      	vm.$forceUpdate();
		      });
		},
		uploadPattern(e){
			e.preventDefault();

		      let main = this;
		      main.cloading = true;

		      var updateForm = new FormData();
		      var vd = new VueDiana();

		      for(var input in this.patternNew){
		        updateForm.append(input,this.patternNew[input]);
		      }

		      updateForm.append("request","pattern");
		      updateForm.append("action","new");
		      updateForm.append("method","post");

		      console.log(this.patternNew);

		      axios.post('asset',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		      }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errorsDp = [];

		             switch(call){

		               case true:
		                 vd.toastSuccess();
		                 main.loadMaterial();
		                 window.dispatchEvent(new Event("reloadCollections"));
		                 main.$forceUpdate();

		               break;

		               case false:
		                 switch(response.data.errors){

		                   case "NotEnoughForm":
		                     main.errorsDp.push({message:"Must fill all fields in form"});
		                   break;

		                   case "NotFileMoved":
		                   	 main.errorsDp.push({message:"Couldn't move file to server"});
		                   break;

		                   case "DPAlready":
		                   	 main.errorsDp.push({message:"That pattern already exist"});
		                   break;

		                   default:
		                     vd.toastError();
		                   break;
		                 }
		               break;
		               
		               default:
		                 vd.toastError();
		               break;
		             }
		        }).catch(function (response) {
		            //handle error
		            console.log(response);
		        }).then(function () {
		        main.cloading = false;
		      });

		  
		},
		loadMaterial(){
			 var main = this;
			 main.materials =[];
			 axios({
		        method: 'get',
		        url: 'asset?r=material&a=da',
		      }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
		               if(response.data.hasOwnProperty("info")){
		                  main.materials = response.data.info;

		                  for (var m in main.materials) {
		                  	main.materialsAutocomplete[m] = {
		                  		text: main.materials[m].title,
		                  		id: main.materials[m].id
		                  	}
		                  }
		               }else{
		                  main.materials = [];
		               }
		              
		             break;
		             
		             default:
		               vd.toastError();
		             break;
		           }
		      }).catch(function (response) {
		          //handle error
		          console.log(response);
		      }).then(function () {
		      	vm.$forceUpdate();
		      });
		},
		uploadMat(e){
			e.preventDefault();

		      let main = this;
		      main.mloading = true;

		      var updateForm = new FormData();
		      var vd = new VueDiana();

		      for(var input in this.materialNew){
		        updateForm.append(input,this.materialNew[input]);
		      }

		      updateForm.append("request","material");
		      updateForm.append("action","new");
		      updateForm.append("method","post");

		      console.log(this.materialNew);

		      axios.post('asset',updateForm,{
		        headers: {
		           'content-type': 'multipart/form-data'
		        }
		      }).then(function (response) {
		            //handle success
		            console.log(response);
		            console.log(response.data);

		            let call = response.data.call;

		            main.errorsMaterial = [];

		             switch(call){

		               case true:
		                 vd.toastSuccess();
		                 main.loadMaterial();
		                 window.dispatchEvent(new Event("reloadMaterials"));
		                 main.$forceUpdate();

		               break;

		               case false:
		                 switch(response.data.errors){

		                   case "NotEnoughForm":
		                     main.errorsMaterial.push({message:"Must fill all fields in form"});
		                   break;

		                   case "NotFileMoved":
		                   	 main.errorsMaterial.push({message:"Couldn't move file to server"});
		                   break;

		                   case "AssetAlready":
		                   	 main.errorsMaterial.push({message:"That material already exist"});
		                   break;

		                   default:
		                     vd.toastError();
		                   break;
		                 }
		               break;
		               
		               default:
		                 vd.toastError();
		               break;
		             }
		        }).catch(function (response) {
		            //handle error
		            console.log(response);
		        }).then(function () {
		        main.mloading = false;
		      });

		  
		}

	}
});