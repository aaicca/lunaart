const EULAComponent = Vue.component("eula",{
	data(){
		return {

		}
	},
 	template: `<div class='nav-padder'>
					<b-container fluid>
						<b-row>
							<b-col>
								<h4 style="color: #1E2446;" class="font-boldy elmessiri gold-flat-text component-title">End User License Agreement </h4>
							</b-col>
						</b-row>
						<b-row>
							<b-col offset=2 sm=8 class="eula-page text-justify" id="display-eula">
								Loading...
							</b-col>
						</b-row>			
					</b-container>
				</div>`,
	mounted(){
		this.loadEULA();
	},
	methods: {
		loadEULA(){
			var main = this;

		    axios({
		        method: 'get',
		        url: 'eula?g=eula',
		    }).then(function (response) {
		          //handle success
		          console.log(response);
		          console.log(response.data);

		          let call = response.data.call;

		           switch(call){

		             case true:
						document.getElementById('display-eula').innerHTML = "<p>"+response.data.info[0].content+"</p>";
		             break;

		             case false:
		               switch(response.data.errors){

		                 default:
						   main.vd.toastError();
						   main.currentEula = true;
		                 break;
		               }
		             break;
		             
		             default:
					   main.vd.toastError();
					   main.currentEula = true;
		             break;
		           }
		    }).catch(function (response) {
		          //handle error
		          console.log(response);
		    });
		}
	}
});