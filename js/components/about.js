const AboutComponent = Vue.component("about",{
	data(){
		return {}
	},
 	template: `<div class='nav-padder'>
 	<b-container fluid>
				  <b-row class="padding-top">
				  	<b-col offset-sm=4 sm=4 offset-lg=5 lg=2 class="text-center">	
						<h4 class='elmessiri font-boldy gold-flat-text main-title main-title-underline'>About</h4>
				  	</b-col>
				  
				  </b-row>
				  <b-row class="text-justify padding luna-text-lightGrey about-p">
				  	<p>We are committed to producing the highest quality of custom stone work and mosaics for commercial and
					residential projects, crafted by hand, from the finest materials around the world.</p>

					<p>Whether fabricating a lavish backsplash or one of a kind medallion, our staff works around the clock in
making clients’ dreams a reality. By assisting you throughout the design and fabrication process, we keep
close contact every step of the way as you watch your design come alive.</p> <br>
					<p>We produce every order to become timeless pieces of art using the latest technology at our disposal. CNC
and waterjet machines assist us to create endless varietiesn and possible aplication to meet clients’
needs. Our products are available through hundreds of exclusive outlets across the country and featured
internationally in hotels, retail stores, shopping centers, office buildings, hospitals, stadiums, museums
and prominent residential homes. Many of these installations have been featured in national publications,
highlighting the individuality and absolute quality of our products.</p> <br>
					<p>We have a unique reputation in design services and an in house installation team that will be able to handle
any intricate installation installation project you may have. Our strict fabrication procedures and quality
control system allow us to offer only premium quality products.</p> <br>
					<p>Commitment and dedicstion to excellence extends beyond our production capabilities with a personalized
approach that begins at the initial quotation and follows through even after your order has been received.
The skill, passion and pride of our team members and dedicated customer service representatives are
ready to handle all of your potential needs and requirements.</p>

				  </b-row>
				  <b-row>
					<b-col class="text-center">
<<<<<<< HEAD
						<br><br>
=======
>>>>>>> 140a1460941b0126e980aabd425ea6008ae44ce7
						<b-button class='btn-gold' @click="openCt">START DISCOVERING <i class="fas fa-chevron-right"></i></b-button>
					</b-col>
				  </b-row>
				</b-container>
</div>`,
	methods: {
		openCt(){
			this.$root.updateRoute("catalog");
		}
	}
});