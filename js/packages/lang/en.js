var lang = {
	"index": {
		"form": {
			"message": {
				"registerFail": "Registration has failed, try again later",
				"already": "If you already have an account",
				"forgotPass": "Forgot you password?",
				"loginFalseUser": "User field is empty",
				"loginFalsePass": "You need your password"
			},
			"title": {
				"login": "Login",
				"signUp": "Sign up"
			},
			"input": {
				"name": "Name",
				"lastname": "Lastname",
				"email": "Email",
				"user": "Email, phone or nickname",
				"password": "Password"
			},
			"button": {
				"facebook": "Login with facebook",
				"signUp": "Sign up",
				"login": "Login",
				"enter": "Enter"
			},
		}	
	},
	"dashboard":{
		"sideNav": {
			"notes": "Notes",
			"lists": "Lists",
			"reminders": "Reminders",
			"trash": "Trash",
			"themes": "Themes",
			"settings": "Settings",
			"logout": "Log out"
		},
		"note": {
			"title": "Untitled",
			"content": "Write Something...",
			"cancel": "Cancel",
			"save": "Save"
		}
	},
	"gMessage": {
		"incompleteForm": "Please fill all the fields",
		"error": "Something went wrong, please try again later",
		"redirecting": "Redirecting",
		"done": "Done"
	}
}