<?php 
	//CORS Policy declatarion

    /* Remember idiots, the admin validation has to be individual cause some resources are loaded from landing page without account */

	if(empty($_GET) && empty($_POST)){
		include "dashboard.php";
	}else{
		session_start();
		header('Access-Control-Allow-Origin: *');  
	    header('Content-Type: application/json;charset=utf-8');
	    header('Vary: Accept-Encoding');

	    require_once(__DIR__ . '/vendor/autoload.php');

		include("model/connection.php");
		include("model/FriendofMySQL.php");
		include("controller/classes/luball.php");
		include("controller/classes/lfile.php");
		include("controller/classes/luballcg.php");


		$db = new FriendofMySQL($connection);
		$lb = new Luball();
		$lf= new lFile();
		$callback = array();
		$errors = array();

		if(!empty($_GET)){
			$method = "GET";
		}else{
			$method = (isset($_POST["method"]) ? $_POST["method"] : "400");
		}

		$lb->power_session_start();

		switch (strtoupper($method)) {
			case 'GET':
			
				
				switch ($_GET["g"]) {

					case "eula":
						$getEULA = "SELECT * FROM eula ORDER BY dated DESC LIMIT 1";

						$eulast = $db->query($getEULA,true);

						if($eulast["status"]){
							$lb->toClient(true,$eulast["query"]);
						}else{
							$lb->defaultQuery();
						}
					break;

					case 'carousel':
						$sql = "SELECT * FROM `carousel`";

						$carousel = $db->query($sql,true);

						if($carousel["status"]){
							$lb->toClient(true,$carousel["query"]);
						}else{
							$lb->defaultQuery();
						}
						
					break;

					case 'cards':
						$sql = "SELECT * FROM `card` LIMIT 4";

						$carousel = $db->query($sql,true);

						if($carousel["status"]){
							$lb->toClient(true,$carousel["query"]);
						}else{
							$lb->defaultQuery();
						}
						
					break;

					case 'catalogs':
						$sql = "SELECT * FROM `catalog` LIMIT 5";

						$catalog = $db->query($sql,true);

						if($catalog["status"]){
							$lb->toClient(true,$catalog["query"]);
						}else{
							$lb->defaultQuery();
						}
						
					break;

					case 'emails':
						if(!$lb->isAdmin()){
							$lb->defaultPermission();
							return false;
						}	

						$sql = "SELECT *,DATE_FORMAT(dated, '%m - %d  - %y') as fdated FROM `mails`";

						$carousel = $db->query($sql,true);

						if($carousel["status"]){
							$lb->toClient(true,$carousel["query"]);
						}else{
							$lb->defaultQuery();
						}
						
					break;
					
					case "d":

						$fullSearch = array();	

						//Orders query

						$OrdersSQL = "SELECT *, LPAD(`number`, 6, '0') as `number`, orders.id as id, DATE_FORMAT(orders.dated, '%m - %d  - %y') as fdated, DATE_FORMAT(orders.receivedDate, '%m/%d/%y') as rdated, DATE_FORMAT(orders.approvalDate, '%m/%d/%y') as adated  FROM orders WHERE orders.client = ".$_SESSION['user']['id']." AND orders.number LIKE '%".$_GET['s']."%' LIMIT 10";

						$orders = $db->query($OrdersSQL,true);

						foreach($orders["query"] as $key => $value) {

							$invQ = "SELECT * FROM invoice WHERE `number` = ".$orders["query"][$key]["number"]." LIMIT 1";

							$orders["query"][$key]["content"] = json_decode($orders["query"][$key]["content"],true);

							$invTry = $db->query($invQ,true);

							if(array_key_exists(0,$invTry["query"]) ){
								$orders["query"][$key]["invoice"] = $invTry["query"][0];
							}else{
								$orders["query"][$key]["invoice"] = NULL;
							}
						}
						
						$fullSearch["orders"] = $orders["query"];

						//Tracks

						$TrackingSQL = "SELECT *, LPAD(packing.`number`, 6, '0') as `number`,  DATE_FORMAT(packing.dated, '%m - %d  - %y') as fdated FROM packing WHERE packing.client = ".$_SESSION['user']['id']." AND (packing.track LIKE '%".$_GET['s']."%' OR packing.number LIKE '%".$_GET['s']."%') LIMIT 10";

						$list = $db->query($TrackingSQL,true);

						$fullSearch["trackings"] = $list['query'];

						//Collections

						$CollectionSQL = "SELECT *, DATE_FORMAT(collection.dated, '%m - %d  - %y') as fdated FROM collection WHERE (collection.materialsStr LIKE '%".$_GET['s']."%' OR collection.title LIKE '%".$_GET['s']."%') LIMIT 10";

						$collection = $db->query($CollectionSQL,true);

						$fullSearch["collections"] = $collection['query'];
						
						//Return search
						$lb->toClient(true,$fullSearch);

					break;

					default:
						$lb->defaultRequest();
					break;
				}
				

			break;

			case 'POST':
				switch ($_POST['request']) {

					case "updateEULA":
						if($lb->isAdmin()){
							extract($_POST);
							$id = $_SESSION['user']['id'];
							$content = nl2br($content);

							$sqlEULA = "INSERT INTO eula (content,
														updater) 
												VALUE ('$content',
														'$id')";

							$sqlUserUpdate = "UPDATE user SET eula = 0 WHERE role >= 2";
							$db->query($sqlUserUpdate,false);

							$eulinst = $db->query($sqlEULA,false);

							if($eulinst["status"]){
								$lb->toClient(true);
							}else{
								$lb->defaultQuery();
							}
						}
						
					break;

					case "addEmail":

						extract($_POST);

						if(!empty($email)){
							if(filter_var($email, FILTER_VALIDATE_EMAIL)){

								$sql = "INSERT INTO mails (title,description,email)
												   VALUES ('$title',
														   '$description',
														   '$email')";

								$set = $db->query($sql,false,true);

								if($set["status"]){
									$lb->toClient(true);
								}else{
									$lb->defaultQuery();
								}

							}else{
								$lb->toClient(false,$callback,"InvalidEmail");
							}
						}else{
							$lb->defaultEnough();
						}
					break;

					case "deleteEmail":

						if($lb->isAdmin()){
							$sql = "DELETE FROM mails WHERE id = ".$_POST['id'];

							$del = $db->query($sql,false,true);	

							if($del['status']){
								$lb->toClient(true);
							}else{
								$lb->defaultQuery();
							}
						}
					break;

					case 'add':

						if($lb->isAdmin()){
							unset($_POST['img']);

							extract($_POST);

							if(!isset($_FILES['file'])){
								$lb->toClient(false,$callback,"NoSlide");
								return false;
							}

							if($id == null){
								$sql = "INSERT INTO carousel (title,
														  content)
													VALUES('$title',
														   '$content')";
							}else{
								$sql = "UPDATE carousel
										SET title = '$title',
											content = '$content'
										WHERE id = ".$id;
							}
							
							//echo $sql;

							$set = $db->query($sql,false,true);
	
							//print_r($set);

							if($set["status"]){
								
								if($id == null){
									$img = $lf->mf($_FILES["file"],"media/slides",$set['last_id'].'.webp');
									$sQlimg = "UPDATE carousel SET img = '".$img["dir_name"]."' WHERE id = ".$set["last_id"];
								}else{
									$img = $lf->mf($_FILES["file"],"media/slides",$id.'.webp');
									$sQlimg = "UPDATE carousel SET img = '".$img["dir_name"]."' WHERE id = ".$id;
								}
								
								//echo $sQlimg;

								$db->query($sQlimg,false,true);

								$lb->toClient(true);
							}else{
								$lb->defaultQuery();
							}
						}else{
							$lb->defaultPermission();
						}

						

					break;

					case "col-update":

						$query[0]["status"] = true;
						$query[1]["status"] = true;
						$query[2]["status"] = true;
						$query[3]["status"] = true;

						if(isset($_FILES["collections"])){
							$img = $lf->mf($_FILES["collections"],"media/cards",'0.webp');

							$sql = "UPDATE card SET img = '".$img["dir_name"]."' WHERE id = 1";

							$query[0] = $db->query($sql,false,true);
						}

						if(isset($_FILES["materials"])){
							$img = $lf->mf($_FILES["materials"],"media/cards",'1.webp');

							$sql = "UPDATE card SET img = '".$img["dir_name"]."' WHERE id = 2";

							$query[1] = $db->query($sql,false,true);
						}

						if(isset($_FILES["shop"])){
							$img = $lf->mf($_FILES["shop"],"media/cards",'2.webp');

							$sql ="UPDATE card SET img = '".$img["dir_name"]."' WHERE id = 3";

							$query[2] = $db->query($sql,false,true);
						}

						if(isset($_FILES["availability"])){
							$img = $lf->mf($_FILES["availability"],"media/cards",'3.webp');

							$sql ="UPDATE card SET img = '".$img["dir_name"]."' WHERE id = 4";

							$query[3] = $db->query($sql,false,true);
						}

						if($query[0]["status"] && $query[1]["status"] && $query[2]["status"] && $query[3]["status"]){
							$lb->toClient(true);
						}else{
							$lb->defaultQuery();
						}

					break;	

					case "v-update":

						if(isset($_FILES["video"])){
							$video = $lf->mf($_FILES["video"],"media/video",'video.mp4');

							print_r($video);

							$sql = "UPDATE card SET img = '".$video["dir_name"]."' WHERE id = 4";

							$query = $db->query($sql,false,true);

							if($query["status"]){
								$lb->toClient(true);
							}else{
								$lb->defaultQuery();
							}
						}else{
							$lb->defaultEnough();
						}

					break;	

					case "catalog-update":

						if(isset($_FILES["catalog"])){
							$pdf = $lf->mf($_FILES["catalog"],"media/catalog",$_POST['category'].'.pdf');

							$img = $lf->mf($_FILES["img"],"media/catalog",$_POST['category'].'.webp');

							$sql = "UPDATE catalog SET pdf = '".$pdf["dir_name"]."', img = '".$img['dir_name']."' WHERE id = ".$_POST['category'];

							$query = $db->query($sql,false,true);

							if($query["status"]){
								$lb->toClient(true);
							}else{
								$lb->defaultQuery();
							}
						}else{
							$lb->defaultEnough();
						}

					break;

					case "delete":
						$lb->power_session_start();

						if($lb->isAdmin()){
							$sql = "DELETE FROM carousel WHERE id = ".$_POST['id'];

							$del = $db->query($sql,false,true);	

							if($del['status']){
								$lb->toClient(true);
							}else{
								$lb->defaultQuery();
							}
						}
						
					break;
					
					default:
						$lb->defaultRequest();
					break;
				}
			break;

			case 'PUT':
				//Post handled like PUT

			break;

			case 'DELETE':
				//Post handled like Delete

			break;
			
			default:
				$lb->defaultMethod($method);
			break;
		}

		$db->close($connection);

		//$db = new FriendofMySQL($execute);
	} 
	//print_r($_GET);
	//Classic HTTP method request hangling

	//Third FOM MySQL Server connection 
	

?>