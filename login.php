<?php 
	//CORS Policy declatarion

    //print_r(empty($_GET));
	/*
	var_dump($_SERVER['REQUEST_METHOD']);
	echo "<br> GET dump: <br>";
	var_dump($_GET);
	echo "<br> POST dump: <br>";
	var_dump($_POST);
	echo "<br>";
	echo "Array POST: ".var_dump(empty($_POST));
	echo "<br>";
	echo "Array Get: ".var_dump(empty($_GET));
	echo "<br>";*/

	if(empty($_GET) && empty($_POST)){
		include "index.php";
	}else{
		session_start();
		header('Access-Control-Allow-Origin: *');  
	    header('Content-Type: application/json;charset=utf-8');
	    header('Vary: Accept-Encoding');

		include("model/connection.php");
		include("model/FriendofMySQL.php");
		include("controller/classes/luball.php");
		include("controller/classes/lfile.php");
		include("controller/classes/luballcg.php");

		$db = new FriendofMySQL($connection);
		$lb = new Luball();
		$lf = new lfile();
		$callback = array();
		$errors = array();

		if(!empty($_GET)){
			$method = "GET";
		}else{
			$method = (isset($_POST["method"]) ? $_POST["method"] : "400");
		}

		switch (strtoupper($method)) {
			case 'GET':
			
				//Get request handler

				switch ($_GET["r"]) {
					case "gs":
						try{
							$lb->toClient(true,$_SESSION);
						}catch(Exception $e){
							$lb->toClient(false,$callback,"NoSession");
						}
					break;
					
					default:
						$lb->defaultRequest();
					break;
				}

			break;

			case 'POST':
				//Post request handler
				switch($_POST["request"]){
					case "signupCompany":
						
						if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS) && isset($_FILES["signature"])){
							if($db->isValue("user","email",$_POST["contactEmail"])){
								$lb->toClient(false,$callback,"EmailExist");
								return false;
							}
							extract($_POST);
							
								/*
    [request] => signupCompany
    [method] => post
								*/

							$temp = $lf->mf($_FILES["signature"],"files/signature/".$lb->encrypt(time().$companyName)."/","signature.svg");
							$temp = $temp["dir_name"];
							
							$sqlCompany = "INSERT INTO company (companyName,
																dba,
																address,
																city,
																state,
																zp,
																phone,
																fax,
																website,
																accountingContact,
																accountingPhone,
																accountingEmail,
																authorizedSalesStaff,
																signature,
																printedName) 
														VALUES ('$companyName',
																'$dba',
																'$businessAddress',
																'$city',
																'$state',
																'$companyZp',
																'$phone',
																'$fax',
																'$website',
																'$contactAccounting',
																'$accountingPhone',
																'$accountingEmail',
																'$authorizedSalesStaff',
																'$temp',
																'$printedName')";



							$sqlShipping = "INSERT INTO shipping (address,
																city,
																state,
																zp,
																contact,
																phone) 
														VALUES ('$shippingAddress',
																'$shippingCity',
																'$shippingState',
																'$shippingZp',
																'$shippingContact',
																'$shippingPhone')";

							$companyQuery = $db->query($sqlCompany,false,true);
							$companyId = $db->lastId();

							$shippingQuery = $db->query($sqlShipping,false,true);
							$shippingId = $db->lastId();

							if($shippingQuery["query"]["status"] == 1 && $shippingQuery["query"]["status"] == 1){
								$sqlUser = "INSERT INTO user (id,
							 						  name, 
							 						  lastname, 
													  business,
							 						  email, 
							 						  pass, 
							 						  phone, 
							 						  address, 
							 						  city, 
							 						  state, 
							 						  zp, 
							 						  role, 
							 						  hash,
													  title,
													  companyId,
													  shippingId,
							 						  dated) 
											VALUES (NULL, 
												   '$contactName', 
												   '$contactLastname', 
												   '$companyName',
												   '$contactEmail',
												   '".$lb->encrypt('AlmsnS1239ssMzz_swnfn!212@')."',
												   '$contactPhone', 
												   '$businessAddress', 
												   '$city', 
												   '$state',
												   '$companyZp', 
												   2, 
												   NULL, 
												   '$contactTitle',
												   '$companyId',
												   '$shippingId',
												   CURRENT_TIMESTAMP)";
									
									$userQuery = $db->query($sqlUser,false,true);
									$userId = $db->lastId();
									if($userQuery["query"]["status"] == 1){
										$db->query("UPDATE `company` SET `owner` = ".$userId." WHERE `company`.`id` = ".$companyId,false);
										$db->query("UPDATE `shipping` SET `owner` = ".$userId." WHERE `shipping`.`id` = ".$shippingId,false);
										$lb->successRequest();
									}else{
										$db->query("DELETE FROM `shipping` WHERE `shipping`.`id` = ".$shippingId,true);
										$db->query("DELETE FROM `company` WHERE `shipping`.`id` = ".$companyId,true);
										$lb->toClient(false);
									}
							}else{
								$db->query("DELETE FROM `shipping` WHERE `shipping`.`id` = ".$shippingId,true);
								$db->query("DELETE FROM `company` WHERE `shipping`.`id` = ".$companyId,true);
								$lb->toClient(false);
							}
							
						}else{
							$lb->toClient(false,$callback,"NotEnoughForm");
						}
					break;

					case "restore":
						//

						if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS)){

							$insQ = "SELECT email FROM user WHERE email = '".$_POST["email"]."' LIMIT 1";
							$getting = $db->query($insQ,true,true);	

							if($getting["num_rows"] >= 1){
								$hash = rand(99999, 999999);
								$insQ = "UPDATE user SET hash = '".$hash."' WHERE email = '".$_POST["email"]."';";

								$setting = $db->query($insQ,false,true);
								if($setting["query"]["status"] == 1){
									if($lb->sendEmail("noreply@urbancitydesigns.com",$_POST["email"],"Account recovery","This is your recovery code: \n".$hash)){
										$lb->toClient(true);
									}else{
										$lb->toClient(false,$callback,"CantEmail");
									}
								}else{
									$lb->toClient(false,$callback,"CantHash");
								}

							}else{
								$lb->toClient(false,$callback,"NotEmail");
							}

							
						}else{
							$lb->toClient(false,$callback,"NotEnoughForm");
						}
					break;

					case "code":

						if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS)){

							$insQ = "SELECT id FROM user WHERE email = '".$_POST["email"]."' AND hash = '".trim($_POST["code"])."'";

							$getting = $db->query($insQ,true,true);	

							if($getting["num_rows"] >= 1){
								$lb->power_session_start();
								$_SESSION["restore"] = true;
								$lb->toClient(true);

							}else{
								$lb->toClient(false,$callback,"NotCode");
							}

							
						}else{
							$lb->toClient(false,$callback,"NotEnoughForm");
						}
					break;

					case "reset":

						$lb->power_session_start();
						
						if(isset($_SESSION["restore"])){
							if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS)){

								if($_POST["pass1"] !== $_POST["pass2"]){

									$lb->toClient(false,$callback,"NotPassMatch");

									return false;
								
								}

								$insQ = "UPDATE user SET pass = '".$lb->encrypt($_POST["pass1"])."' WHERE email = '".$_POST["email"]."'";

								$getting = $db->query($insQ,false,true);	

								if($getting["query"]["status"] >= 1){
									if(session_id() == ""){
										$lb->power_session_start();
										session_destroy();
									}else{
										session_destroy();
									}
									
									$lb->toClient(true);

								}else{
									$lb->toClient(false,$callback,"NotQuery");
								}

								
							}else{
								$lb->toClient(false,$callback,"NotEnoughForm");
							}
						}else{
							$lb->toClient(false,$callback,"RestoreExpired");
						}

					break;	

					case "login":
						
						if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS)){

							$info = $db->newSession($_POST["user"],$_POST["pass"]);

							if($info["status"] !== true){
								$info["user"] = array();

								$lb->toClient(false,$info["user"],$info["status"]);
							}else{
								$lb->toClient(true,$_SESSION);
							}

							
						}else{
							$lb->toClient(false,$callback,"NotEnoughForm");
						}
					break;

					case "signup":

						unset($_POST["fmessageC"]);

						if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS)){

							if($db->isValue("user","email",$_POST["email"])){
								$lb->toClient(false,$callback,"EmailExist");
								return false;
							}

							if($_POST["fpass"] !== $_POST["spass"]){

								$lb->toClient(false,$callback,"NotPassMatch");

								return false;
							
							}
						
							extract($_POST);

							$insQ = "INSERT INTO user (id,
							 						  name, 
							 						  lastname, 
							 						  email, 
							 						  pass, 
							 						  phone, 
							 						  address, 
							 						  city, 
							 						  state, 
							 						  zp, 
							 						  role, 
							 						  hash, 
							 						  dated) 
											VALUES (NULL, 
												   '$fname', 
												   '$sname', 
												   '$email',
												   '".$lb->encrypt($fpass)."',
												   '$phone', 
												   '$address', 
												   '$city', 
												   '$stateS',
												   '$zp', 
												   3, 
												   NULL, 
												   CURRENT_TIMESTAMP)";

							$sign = $db->query($insQ,false,true);

							if($sign["query"]["status"]){
								$lb->toClient(true);	

								$db->setNotification("New user","Name: ".$fname." ".$sname." <br> Email: ".$email." <br> Phone: ".$phone,0,0,$_POST);

								$db->newSession($email,$fpass);
							}
							
						}else{

							$lb->toClient(false,$callback,"NotEnoughForm");

							return false;
						}
						
					break;

					case "contact":
						unset($_POST["fpass"]);
						unset($_POST["spass"]);

						$_POST["fmessageC"] = (empty($_POST["fmessageC"]) ? $_POST["fmessageC"] : "Not message");

						if($lb->isSEA($_POST,DEFAULT_EXCEPTIONS)){

							extract($_POST);

			
							if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){
								$lb->toClient(false,$callback,"InvalidEmail");
								return false;
							}

							$message = "Somebody has made contact with us:\n\n$fname\n$email\n$phone\n\n$address\n$city\n$stateS\n$zp\n$fmessageC";

							$sqlMails = "SELECT * FROM mails";

								$mails = $db->query($sqlMails,true);

								
								$escapeMails = array("admin@urbancitydesigns.com");

								foreach ($mails["query"] as $key => $value) {
									array_push($escapeMails,$mails["query"][$key]["email"]);
 								}
								
								foreach ($escapeMails as $key => $value) {
									$lb->sendEmail("no-reply@urbancitydesigns.com",$value,"New contact",$message);
								}

							
								$lb->successRequest();
						}else{

							$lb->toClient(false,$callback,"NotEnoughForm");

							return false;
						}
					break;
					
					default:
						$lb->defaultRequest();
					break;
				}
			break;

			case 'PUT':
				//Post handled like PUT
			break;

			case 'DELETE':
				//Post handled like Delete
			break;
			
			default:
				$lb->defaultMethod($method);
			break;
		}

		$db->close($connection);

		//$db = new FriendofMySQL($execute);
	} 
	//print_r($_GET);
	//Classic HTTP method request hangling

	//Third FOM MySQL Server connection 

?>