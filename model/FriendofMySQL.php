<?php /*namespace friendofmysql;*/
/*FriendofMySQL 1.0.2*/

/*  Three parameters That can make magic $pass (password of the friendofmysql library ), $cmd (Command what you want to do), $data (The data if you are going to insert data if it is not left blank). */

class FriendofMySQL{

	/* properties of data outputs | Return call */
	public $callback;
	public $connection;

                /* example. $pass = friendofmysql , $cmd = insert_animals */
	public function __construct($connection){

	    try{   
	    		header('Content-Type: application/json;charset=utf-8');
				//connection and name of the database
				include("connection.php"); /*batch connection*/
				
				$this->connection = $connection;

				//parse_str($data["request"],$data["request"]);
				
				/*if(isset($data["form"])){
					parse_str($data["form"],$data["form"]);
				}*/
				
				//Data entry security protection

				if(!empty($connection)){

					//$cq();
					//self::cq($connection,$cq,true);

					//$file="query/query.php";
					//include "MySQL.dll";

					/*Close connection*/
					/*if($CONFIG['connection']['type']=="MySQLi_Object"){
					  $connection->close();
					}else if($CONFIG['connection']['type']=="MySQLi_Procedural"){
					  mysqli_close($connection);
					}else if($CONFIG['connection']['type']=="PDO"){
					  $connection = null;
					}*/

				}else{
				  throw new Exception('<br> 200 Error of classes');/*200 Error of clases*/
				}

		}catch(Exception $e){

			echo $e->getMessage(); /*Return error*/

		}

	            
	}

	public function __destruct(){
		return true;

	}

	public function close($connection){
		$connection->close();
	}

	public function testAPI(){
		$this->callback = '{"lalas":"lorem"}';
		return $this->callback;
	}

	public function queryOneValue($table,$search,$key,$mainly,$str=false,$debug = false){

		/*
			$table: Table
			$seach: Key value we want
			$key: Prefferable id, but is the key to search
			$mainly: value to match
		*/

		$connection = $this->connection;

		if($str){
			$query = "SELECT ".$search." FROM `".$table."` WHERE `".$key."` = '".$mainly."' LIMIT 1";
		}else{
			$query = "SELECT ".$search." FROM `".$table."` WHERE `".$key."` = ".$mainly." LIMIT 1";
		}

		

		if($debug){
			$data = $connection->query($query) or die($connection->error);
		}else{
			$data = $connection->query($query);
		}
		
		
		$data = $data->fetch_all(MYSQLI_ASSOC);
		$flag = false;

		if(isset($data[0][$search])){
			$flag = $data[0][$search];
		}

		return $flag;
	}

	public function query($sql,$qt=true,$debug=false){
		//qt true = info retrieve | qt false = only an boolean confirmation like an update or insert
		//
		$connection = $this->connection;

		if($debug){
			$data = $connection->query($sql) or die($connection->error);
		}else{
			$data = $connection->query($sql);
		}
		
		$array = array();

		if($qt){
			if($data){
				$array["query"] = $data->fetch_all(MYSQLI_ASSOC);
				$array["num_rows"] = $data->num_rows;
				$array["status"] = true;
			}else{
				$array["status"] = false;
				$array["num_rows"] = 0;
				$array["query"] = $array;
			}
		}else{
			if($data){
				$array["status"] = true;
				$array["last_id"] = $connection->insert_id;
				$array["query"] = $array;
			}else{
				$array["status"] = false;
				$array["last_id"] = $connection->insert_id;
				$array["query"] = $array;
			}
		}
		
		return $array;
	}

	public function retrieverLite($callback,$debug = false){
		$this->callback = $callback;
		return true;
	}

	public function lastId(){
		return $this->connection->insert_id;
	}

	function isValue($table,$key,$value,$debug=false){
		$connection = $this->connection;

		if($debug){
			$data = $connection->query("SELECT id FROM ".$table." WHERE ".$key." = '".$value."'") or die($connection->error);
		}else{
			$data = $connection->query("SELECT id FROM ".$table." WHERE ".$key." = '".$value."'");
		}
		


		if($data->num_rows >= 1){
			return true;
		}else{
			return false;
		}
		
		return $array;
	}

	public function retriever($connection,$sql,$queryType = true,$debug = false){
		//queryType true = info retrieve | queryType false = only an boolean confirmation
		if($debug){
			$data = $connection->query($sql) or die($connection->error);
		}else{
			$data = $connection->query($sql);
		}
		
		$array = array();

		if($queryType){
			if($data){
				$array = $data->fetch_all(MYSQLI_ASSOC);
				$array["status"] = "true";

				$this->callback = $array;
			}else{
				$array["status"] = "false";
				$this->callback = $array;
			}
		}else{
			if($data){
				$array["status"] = "true";
				$this->callback = $array;
			}else{
				$array["status"] = "false";
				$this->callback = $array;
			}
		}
		

		return $array;
	}

	public function setNotification($title,$content,$owner,$type=0,$data=""){
		$connection = $this->connection;
		$lb = new Luball();

		$insQ = "INSERT INTO notification (title, 
											content, 
											data, 
											owner,
											type, 
											dated) 
								VALUES ('".$title."', 
										'".$content."', 
										'".json_encode($data)."', 
										'".$owner."',
										'".$type."',
										CURRENT_TIMESTAMP); ";

		$notification = self::query($insQ,false,true);

		if($notification["status"]){
			return true;
		}else{
			return false;
		}					

	}

	public function randomNumber($min,$max,$table,$key,$debug = false){
		$connection = $this->connection;
		$number = rand($min,$max);

		if($debug){
			$data = $connection->query("SELECT ".$key." FROM ".$table." WHERE ".$key." = '".$number."'") or die($connection->error);
		}else{
			$data = $connection->query("SELECT ".$key." FROM ".$table." WHERE ".$key." = '".$number."'");
		}


		if($data->num_rows >= 1){
			self::randomNumber($min,$max,$table,$key,$debug);
		}else{
			return $number;
		}
	}

	public function newSession($user,$pass){
		$connection = $this->connection;
		$lb = new Luball();
		$answer = array();

		if(!$lb->isSEV($user)){
			$answer["status"] = "EnoughUser";
			return $answer;
		}else if(!$lb->isSEV($pass)){
			$answer["status"] = "EnoughPass";
			return $answer;
		}
		
		$userQuery = $connection->query("SELECT 
											*
										FROM 
											user 
										WHERE 
											email = '$user'
										LIMIT 1") or die($connection->error);


		if($userQuery->num_rows != 1){
			$answer["status"] = "NoUser";
			return $answer;
		}
		
		$answer["user"] = $userQuery->fetch_assoc();

		if($answer["user"]["pass"] != $lb->encrypt($pass)){
			$answer["status"] = "NoPass";
			return $answer;
		}

		unset($answer["user"]["pass"]);

		if($userQuery){
			if(session_status() == PHP_SESSION_NONE) {
				session_start();
			}
			$answer["status"] = true;
			$answer["user"]["status"] = true;

			$_SESSION["user"] = $answer["user"];
		}else{
			$answer["status"] = false;
		}

		return $answer;

		/*$data = $connection->query("SELECT ");

		session_start();
		$_SESSION["sId"] = session_id();
		$_SESSION[]*/
	}
}

#by yolfry
?>