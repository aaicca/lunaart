<?php 
	//CORS Policy declatarion

    //print_r(empty($_GET));
	
	$method = "";
	$page = true;

	if(empty($_GET) && empty($_POST)){
		include "dashboard.php";
	}else{
		session_start();
		header('Access-Control-Allow-Origin: *');  
	    header('Content-Type: application/json;charset=utf-8');
	    header('Vary: Accept-Encoding');

		include("model/connection.php");
		include("model/FriendofMySQL.php");
		include("controller/classes/luball.php");
		include("controller/classes/lfile.php");
		include("controller/classes/luballcg.php");

		$db = new FriendofMySQL($connection);
		$lb = new Luball();
		$lf = new lfile();
		$callback = array();
		$errors = array();

		if(!empty($_GET)){
			$method = "GET";
		}else{
			$method = (isset($_POST["method"]) ? $_POST["method"] : "400");
		}

		switch (strtoupper($method)) {
			case 'GET':
				//Get request handler
				switch ($_GET["g"]) {
					case 'a':

						$sql = "SELECT *, DATE_FORMAT(dated, '%m - %d  - %y') as fdated FROM gallery ORDER BY dated DESC";

						$gallery = $db->query($sql,true,true);

						$lb->toClient(true,$gallery["query"]);
					break;

					default:
						$lb->defaultRequest();
					break;
				}
				
			break;

			case 'POST':
				//Post request handler
				switch ($_POST['request']) {
					case 'add':
						
						$lb->power_session_start();
					
						if($lb->isAdmin()){
							

							if($lb->isSEA($_POST)){
								extract($_POST);

								if(isset($_FILES["file"])){
									
									$name = time();

									if($db->isValue("gallery","title",$title)){
										$lb->toClient(false,$callback,"AlreadyTitle");
										return false;	
									}

									$img = $lf->mf($_FILES["file"] , "media/gallery/".$category , $name.".webp");

									$try = "SELECT materialsStr FROM collection WHERE id = ".$collection." LIMIT 1";

									$qTry = $db->query($try,true);



									$sql = "INSERT INTO `gallery` (`title`, 
																   `collection`, 
																   `materials`, 
																   `img`, 
																   `category`) 
														   VALUES ('$title', 
														   		   '$collection',
														   		    '".$qTry['query'][0]['materialsStr']."', 
														   		   '".$img['dir_name']."', 
														   		   '".$category."')";
									$addG = $db->query($sql,false);

									if($addG["status"]){
										$lb->toClient(true);
									}else{
										$lb->defaultQuery();
									}

								}else{
									$lb->toClient(false,$callback,"NoFile");
								}
							}else{
								$lb->defaultEnough();
								return false;
							}

							
							
						}else{
							$lb->defaultPermission();
						}
					break;
					
					case "delete":
						$lb->power_session_start();

						if($lb->isAdmin()){
							$sql = "DELETE FROM gallery WHERE id = ".$_POST["id"];

							$try = $db->query($sql,false,true);

							if($try["status"]){
								$lb->successRequest();
							}else{
								$lb->defaultQuery();
							}
						}else{	
							$lb->defaultPermission();
						}
					break;	

					default:
						$lb->defaultRequest();
					break;
				}
			break;

			case 'PUT':
				//Post handled like PUT
				$_POST['method'] = 'PUT';
				$method = $_POST;
				$page = false;
			break;

			case 'DELETE':
				//Post handled like Delete
				$_POST['method'] = 'DELETE';
				$method = $_POST;
				$page = false; 
			break;
			
			default:
				echo "{'error':'Bad server request'}";
			break;
		}
	} 
	//print_r($_GET);
	//Classic HTTP method request hangling

	

	//Third FOM MySQL Server connection 

	if(!$page){
		$FriendofMySQL = new FriendofMySQL("root","login",$method); 
		if($FriendofMySQL->callback != "false"){ 
			echo json_encode($FriendofMySQL->callback); 
		}else{ 
			echo "{'error':false}"; 
		} 
	}
	

?>